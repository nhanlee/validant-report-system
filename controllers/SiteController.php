<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\User;
use app\models\ResetPasswordForm;
use app\models\ValidantReportSearch;
use app\models\SourcingReportSearch;
use app\models\ExpenseReportSearch;
use kartik\mpdf\Pdf;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','reset'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout','index','report','export'  ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex(){
        $this->redirect(Yii::$app->homeUrl . '/tasks/index');
    }

    public function actionReport()
    {
        $searchModel = new ValidantReportSearch();
        $searchModel->load(Yii::$app->request->queryParams);
        //Part 1 & 2
        $data = $searchModel->search(Yii::$app->request->queryParams);

        //Part 3
        $sourcingModel = new SourcingReportSearch();
        $sourcingModel->report = 1;
        $sourcingProvider = $sourcingModel->search(Yii::$app->request->queryParams);

        //Part 4
        $expenseModel = new ExpenseReportSearch();
        $expenseModel->report = 1;
        $expenseProvider = $expenseModel->search(Yii::$app->request->queryParams);

        return $this->render('validantreport', [
              'searchModel' => $searchModel,//Part 1 & 2
              'data' => $data,
              'sourcingProvider' => $sourcingProvider,//Part 3
              'expenseProvider' => $expenseProvider,//Part 4
        ]);
    }

    public function actionExport()
    {
        $params = Yii::$app->request->queryParams;

        $searchModel = new ValidantReportSearch();
        $searchModel->load($params['data']);
        //Part 1 & 2
        $data = $searchModel->search($params['data']);

        //Part 3
        if($params['data']){
          $sourcingModel = new SourcingReportSearch();
          $sourcingModel->report = 1;
          $sourcingProvider = $sourcingModel->search($params['data']);

          //Part 4
          $expenseModel = new ExpenseReportSearch();
          $expenseModel->report = 1;
          $expenseProvider = $expenseModel->search($params['data']);
        }

        $content = $this->renderPartial('_validantreport', [
              'searchModel' => $searchModel,//Part 1 & 2
              'data' => $data,
              'sourcingProvider' => $sourcingProvider,//Part 3
              'expenseProvider' => $expenseProvider,//Part 4
        ]);

        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_DOWNLOAD,
            // your html content input
            'content' => $content,
            'filename' => 'validant_work_report_' . $params['data']['ValidantReportSearch']['added'] . '.pdf',
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            // any css to be embedded if required
             // set mPDF properties on the fly
            'options' => ['title' => 'Validant Work Report'],
             // call mPDF methods on the fly
            'methods' => [
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionReset($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
