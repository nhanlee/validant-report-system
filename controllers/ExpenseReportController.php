<?php

namespace app\controllers;

use Yii;
use app\models\ExpenseReport;
use yii\filters\AccessControl;
use app\models\ExpenseReportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Clients;
/**
 * ExpenseReportController implements the CRUD actions for ExpenseReport model.
 */
class ExpenseReportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','report'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SourcingReport models.
     * @return mixed
     */
    public function actionReport()
    {
        $searchModel = new ExpenseReportSearch();
        $searchModel->report = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all ExpenseReport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ExpenseReport();
        if ($model->load(Yii::$app->request->post())) {
            $model->created_by = Yii::$app->user->getId();
            $client = Clients::findOne($model->client_id);
            $model->team = $client->team;
            if($model->save()){
                //Cache sourcer, recruiter and team
                $cache = Yii::$app->cache;
                $cache->set('expenseInput', Yii::$app->request->post());

                Yii::$app->session->setFlash('inputFinish', '<strong>Expense task</strong> has been added successfully!');
                return $this->refresh();
            }
        } else {
            $cache = Yii::$app->cache;
            $oldData = $cache->get('expenseInput');
            if(isset($oldData['ExpenseReport'])){
                if(isset($oldData['ExpenseReport']['beginning'])){
                    $oldData['ExpenseReport']['beginning'] = '';
                }
                if(isset($oldData['ExpenseReport']['done'])){
                    $oldData['ExpenseReport']['done'] = '';
                }
                if(isset($oldData['ExpenseReport']['old'])){
                    $oldData['ExpenseReport']['old'] = '';
                }
            }
            $model->load($oldData);
            $searchModel = new ExpenseReportSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single ExpenseReport model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ExpenseReport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ExpenseReport();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_by = Yii::$app->user->getId();
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ExpenseReport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $client = Clients::findOne($model->client_id);
            $model->team = $client->team;
            $model->updated_by = Yii::$app->user->getId();
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ExpenseReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ExpenseReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ExpenseReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ExpenseReport::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
