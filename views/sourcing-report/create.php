<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SourcingReport */

// $this->title = 'Create Sourcing Report';
// $this->params['breadcrumbs'][] = ['label' => 'Sourcing Reports', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="sourcing-report-create clearfix">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
