<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "newcvs".
 *
 * @property integer $id
 * @property string $name
 * @property integer $taskID
 */
class Newcvs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newcvs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taskID'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'taskID' => 'Task ID',
        ];
    }
}
