$.material.init();

if(typeof(app) != 'undefined'){
	//Check processing and disable or not
	if(app.isProcessing){
		//Auto check my processing
		$currentTrEl = $("#gridtask").find("[data-key='" + app.isProcessing + "']");
		$currentInput = $currentTrEl.find('input[type="checkbox"]');
		$currentInput.prop('checked', true);
		// disable All
		disableAll();
	} else {
		disableAll();
		// just enable Awaiting for checking
		enableAwaiting();
	}

	//Checkbox checked
	$("#gridtask input[type=checkbox]").on("click", function (e) {
		//Please disable me.
		disableMe($(this));

		//Tr data-key is taskID
		$trEl = $(this).parents('tr');
		if($trEl){
			//td status
			$tdStatus = $trEl.find('td.status');
			//Checkbox status is Awaiting?
			if($(this).val() == "0"){
				//If user check this checkbox
				if($(this).prop('checked')){
						$.ajax({
								method: "POST",
								url: "pick",
								async: false,
								data: { taskID: $trEl.data().key},
								success: function(response){
			                	$data = response.data;
												//Change Owner text and status box
			                	$tdOwner = $trEl.find('td.owner');
			                	$tdOwner.text($data.userName);// Change owner
			                	$tdStatus.html($data.statusEl); // Change status
												//Show notification
				                if(response.success){
														app.isProcessing = parseInt($data.taskID);
														//Show upload button
														$tdAction = $trEl.find('td.actions');
														$tdAction.append('<a class="upload" href="javascript:void(0)" title="Upload"><span class="glyphicon glyphicon-upload"></span></a>');

				                		Notify('Success',$data.msg, 'success');
				                		disableAll();
				                } else {
				                		Notify('Information',$data.msg, 'info');
														e.preventDefault();
				                }
				            },
				            failure: function(){

				            }
				        });
				} else {
					//User uncheck checkbox
					// $statusEl.removeClass().addClass("label label-default");
					// $statusEl.text('Awaiting');
				}
			}
		}
	});

	//Event after upload file
	$(document).on('fileuploaded','#input-id', function(event, data, previewId, index) {
			response = data.response;
			if(response.uploaded){
				//update content button layout
				uploadedHtml = response.uploadedHtml;
				$currentTrEl = $("#gridtask").find("[data-key='" + app.isProcessing + "']");
				$currentTrEl.find('td.status').html(uploadedHtml);
				//Show notification
				Notify('Success','Upload file completed', 'success');
				//Show button done
				$tdAction = $currentTrEl.find('td.actions');
	    		$tdAction.children('a.done').remove();//remove button old done button
				$tdAction.append(' <a class="done" href="javascript:void(0)" title="Done"><span class="glyphicon glyphicon-ok-circle"></span></a>');

			}
	});

	//Upload button action click
	$('#gridtask').on("click", "a.upload", function (e) {
			//Reset task for uploading file
			$('#input-id').fileinput('refresh', {uploadExtraData: {'task_id' : app.isProcessing}});
			//Get current selected and show data
			result = $.grep(app.gridData, function(e){ return e.task_id == app.isProcessing; });
			$(".modalUpload .modal-title").text(result[0].task_candidate);

			$(".modalUpload").modal();//Show modal
			e.preventDefault();
	});

	
	//Done button action click
	$('#gridtask').on("click", "a.done", function (e) {
			$trEl = $(this).parents('tr');
			$tdStatus = $trEl.find('td.status');
			var r = confirm("Please check the formatted resume carefully before mark DONE.");
		    if (r == true) {
		        $.ajax({
						method: "POST",
						url: "done",
						async: false,
						data: { taskID: parseInt(app.isProcessing)},
						success: function(response){
		            		$data = response.data;
		                	if(response.success){
				            	$tdStatus.html($data.statusEl); // Change status
								app.isProcessing = 0;//Reset processing
								//Show send button
								$tdAction = $trEl.find('td.actions');
		                		//Remove child
		                		$tdAction.children('a.done').remove();
		                		$tdAction.children('a.upload').remove();

								$tdAction.append('<a class="send" href="javascript:void(0)" title="Send"><span class="glyphicon glyphicon-send"></span></a>');
		                		//Uncheck checkbox
		                		$trEl.find('input[type=checkbox]').prop('checked', false);
								//Show notification
		                		Notify('Congratulations',$data.msg, 'success');

		                		//Enable all awaiting
		                		enableAwaiting();
		                	}
			            }
		        });
		    }
	});

}//end app


//Upload button action click
$('.tasks-view').on("click", "a.oldfile", function (e) {
		$(".modalUpload .modal-title").text('Upload Attachments');
		$('.modalUpload #input-id').fileinput('refresh', {uploadUrl: app.homeUrl + '/cvs/file-upload'});
		$(".modalUpload").modal();//Show modal
		e.preventDefault();
});

$('.tasks-view').on("click", "a.newfile", function (e) {
		$(".modalUpload .modal-title").text('Upload Formatted Resume');
		$('.modalUpload #input-id').fileinput('refresh', {uploadUrl: app.homeUrl + '/newcvs/admin-file-upload'});
		$(".modalUpload").modal();//Show modal
		e.preventDefault();
});

//send button action click
$('#gridtask').on("click", "a.send", function (e) {
		$TrEl = $(this).parents('tr');
		$taskID = $TrEl.data().key;
		$candidateEl = $TrEl.find('td.candidateName');
		$subjectEl = $TrEl.find('td.subject');
		$addressEl = $TrEl.find('td.address');
		//Get current selected and show data
		$(".modalSendmail .modal-title").text($candidateEl.text());
		//Reset form before show.
		$formSendmail =  $('form#sendmail');
		$formSendmail[0].reset();
		$formSendmail.find("#toEmail").val($addressEl.text());
		// $formSendmail.find("#ccEmail").val('james@vsource.io');
		$formSendmail.find("#subjectEmail").val('RE: ' + $subjectEl.text());
		$formSendmail.find("input[type=hidden]").val($taskID);
		$(".modalSendmail").modal();//Show modal
		e.preventDefault();
});

$("form#sendmail").submit(function(e) {
	$(".modalSendmail").modal('hide');//hide
	//show modal loading
	$("#modalLoading").modal({
	  backdrop: 'static',
	  keyboard: false
	});//Show modal

    $.ajax({
		type: "POST",
		url: 'send',
		data: $(this).serialize(), // serializes the form's elements.
		success: function(response)
		{	
			//Hide modal loading
			$("#modalLoading").modal('hide');

			$data = response.data;
        	if(response.success){
        		$sendTrEl = $("#gridtask").find("[data-key='" + $data.taskID + "']");
        		$tdStatus = $sendTrEl.find('td.status');
            	$tdStatus.html($data.statusEl); // Change status
        		//Remove child
				$tdAction = $sendTrEl.find('td.actions');
        		$tdAction.children('a.send').remove();

				//Show notification
        		Notify('Success',$data.msg, 'success');
        	}
		}
	});

    e.preventDefault();
});

//sendTemplate button action click
$('.template-index').on("click", "a.sendTemplate", function (e) {
		$content = "";
		var d = new Date();
		var twoDigitMonth = ((d.getMonth().length+1) === 1)? (d.getMonth()+1) : '0' + (d.getMonth()+1);
		var strDate = twoDigitMonth + "." + d.getDate() + "." + d.getFullYear();

		$TrEl = $(this).parents('tr');
		$type = $TrEl.find('td.type');
		$subject = $TrEl.find('td.subject');
		$content = $TrEl.find('td.content').html();
		$(".modalSendmail .modal-title").text($type.text());
		//Reset form before show.
		$formSendmail =  $('form#sendmailTemplate');
		$formSendmail[0].reset();
		$formSendmail.find("#toEmail").val();
		// $formSendmail.find("#ccEmail").val('james@vsource.io');
		$formSendmail.find("#subjectEmail").val($subject.text() + strDate);
		CKEDITOR.instances.w1.setData( $content.replace(/MM.DD.YY/gi, strDate) );
		$(".modalSendmail").modal();//Show modal
		e.preventDefault();
});
$("form#sendmailTemplate").submit(function(e) {
	var r = confirm("Please check carefully before send email.");
    if (r == true) {
		$(".modalSendmail").modal('hide');//hide
		//show modal loading
		$("#modalLoading").modal({
		  backdrop: 'static',
		  keyboard: false
		});//Show modal

	    $.ajax({
			type: "POST",
			url: 'send',
			data: $(this).serialize(), // serializes the form's elements.
			success: function(response)
			{	
				//Hide modal loading
				$("#modalLoading").modal('hide');

				$data = response.data;
	        	if(response.success){
					//Show notification
	        		Notify('Success',$data.msg, 'success');
	        		CKEDITOR.instances.w1.setData('');//reset CKEDITOR
	        	}
			}
		});
    }

    e.preventDefault();
});

function disableAll(){
	$("#gridtask input[type=checkbox]").attr('disabled','disabled');//disable all
	$("#gridtask div.checkbox").removeClass().addClass("checkbox disabled");
}
function enableAwaiting(){
	$("#gridtask input[type=checkbox][value=0]").prop('disabled', false);
	$("#gridtask input[type=checkbox][value=0]").parents('div.checkbox').removeClass().addClass("checkbox");
}
function enableMe($me){
	$me.prop('disabled', false);
	$me.parents('div.checkbox').removeClass().addClass("checkbox");
}
function disableMe($me){
	$me.prop('disabled', true);
	$me.parents('div.checkbox').removeClass().addClass("checkbox disabled");
}
