<?php

use yii\db\Schema;
use yii\db\Migration;

class m150721_042715_address_task extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `tasks` ADD COLUMN `address` VARCHAR(45) NULL AFTER `recruiter`;";
        $this->execute($sql);
        echo "Done Client" .'\n';
    }

    public function down()
    {
        echo "m150721_042715_address_task cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
