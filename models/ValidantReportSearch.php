<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\entitydb\legacyDB;

/**
 *  represents the model behind the search form about `app\models\SourcingReport`.
 */
class ValidantReportSearch extends Model
{
    /* your calculated attribute */
    public $added;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['added'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
      $validantReport = array();
      if($this->added){
          $tmp = explode(' - ', urldecode($this->added));
          $from = strtotime(trim($tmp[0]) . ' UTC');
          $to = strtotime(trim($tmp[1]) . ' +1 day UTC');

          //PART 1
          $sourcingUpdate = SourcingReport::getValidantReport(trim($tmp[0]) . ' 00:00:00',trim($tmp[1]) . ' 23:59:59');
          $legacyDB = new legacyDB();
          $legacyDB->from = $from;
          $legacyDB->to = $to;

          // Collect data
          $newJobs = $legacyDB->getNewJobs();
          $jobsSubmission = $legacyDB->getJobsHasSubmission();
          $CvsUS = $legacyDB->getJobsHasSubmissionUS();
          $CvsEU = $legacyDB->getJobsHasSubmissionEU();
          $CvsUSNew = $legacyDB->getJobsHasSubmissionUSNew();
          $CvsEUNew = $legacyDB->getJobsHasSubmissionEUNew();
          $CvsAccepted = $legacyDB->getResumesAccepted();
          if($CvsUS > $CvsUSNew){
            $CvsUSNew = $CvsUSNew . ' (' . round($CvsUSNew*100/$CvsUS,1) .'%)' ;
          }
          if($CvsEU > $CvsEUNew){
            $CvsEUNew = $CvsEUNew . ' (' . round($CvsEUNew*100/$CvsEU,1) .'%)' ;
          }

          $data[] = ['des' => 'New jobs open', 'total' => $newJobs];
          $data[] = ['des' => 'Total jobs run', 'total' => $jobsSubmission];
          $data[] = ['des' => 'Resumes sent(US)', 'total' => $CvsUS];
          $data[] = ['des' => 'Resumes sent(EU)', 'total' => $CvsEU];
          $data[] = ['des' => 'New resumes sent(US)', 'total' => $CvsUSNew];
          $data[] = ['des' => 'New resumes sent(EU)', 'total' => $CvsEUNew];
          $data[] = ['des' => 'Resumes accepted', 'total' => $CvsAccepted];

          //Get data for sourcing update
          if($sourcingUpdate){
            foreach ($sourcingUpdate as $value) {
              $data[] = $value;
            }
          }
          $validantReport['part1'] = $data;

          //PART 2
          $part2 = $legacyDB->getPart2();
          $validantReport['part2'] = $part2;
          
          return $validantReport;
      }
    }
}
