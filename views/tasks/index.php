<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use app\models\Tasks;

// use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TasksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;

$gridData = [];

if($dataProvider){
    $models = $dataProvider->getModels();
    foreach ($models as $model) {
        $gridData[] = [
            'task_id' => $model->id,
            'task_candidate' => $model->candidate
        ];

    }
}
?>
<div id="notifications"></div>

<?php  echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="tasks-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'id' => 'gridtask',
        'columns' => [
            [
                'header' => false,
                'class' => 'ext\components\CheckboxColumnCustom',
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    return ['value' => $model->status];
                }
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'attribute' => 'date',
                'filter' => false,
                'headerOptions' =>['width' => '10%'],
                'format' => ['date', 'php:d M, Y']
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'attribute' => 'Recruiter',
                'headerOptions' =>['width' => '15%'],
                'filter' => false,
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->recruiter;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'attribute' => 'Address',
                'filter' => false,
                'headerOptions' =>['class' => 'hidden'],
                'contentOptions' =>['class' => 'address hidden'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->address;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'attribute' => 'Subject',
                'filter' => false,
                'contentOptions' =>['class' => 'subject'],
                'content' => function ($model, $key, $index, $column)
                {
                    return Html::a($model->email->subject, ['view-content', 'email' => $model->email->id], ['target' => '_blank']);
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'attribute' => 'Candidate',
                'filter' => false,
                'contentOptions' =>['class' => 'candidateName'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->candidate;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Cvs',
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->getHtmlCvs();
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'status',
                'headerOptions' =>['class' => 'text-center'],
                'contentOptions' =>['class' => 'text-center status'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->getHtmlStatus();
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Owner',
                'contentOptions' =>['class' => 'owner'],
                'content' => function ($model, $key, $index, $column)
                {
                    $loginUser = $model->loginUser;
                    if($loginUser){
                        if($loginUser->id == Yii::$app->user->getId()){
                            return 'You';
                        }
                        return $loginUser->username;
                    }
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' =>['width' => '8%'],
                'contentOptions' =>['class' => 'actions'],
                'template' => '{view} {update} {upload} {done} {send}',
                'buttons' => [
                   //pick button
                   'upload' => function ($url, $model) {
                       if($model->user == Yii::$app->user->getId() && ($model->status == Tasks::STATUS_PROCESSING || $model->status == Tasks::STATUS_UPLOADED)){
                           return Html::a("<span class='glyphicon glyphicon-upload'></span>", 'javascript:void(0)', [
                               'title' => Yii::t('app', 'Upload'),
                               'class'=>'upload',
                           ]);
                       }
                   },
                   //done button
                   'done' => function ($url, $model) {
                       if($model->user == Yii::$app->user->getId() && $model->status == Tasks::STATUS_UPLOADED){
                           return Html::a("<span class='glyphicon glyphicon-ok-circle'></span>", 'javascript:void(0)', [
                               'title' => Yii::t('app', 'Done'),
                               'class'=>'done',
                           ]);
                       }
                   },
                   //send button
                   'send' => function ($url, $model) {
                       if(($model->user == Yii::$app->user->getId() && $model->status == Tasks::STATUS_DONE) || Yii::$app->user->identity->group == 1){
                           return Html::a("<span class='glyphicon glyphicon-send'></span>", 'javascript:void(0)', [
                               'title' => Yii::t('app', 'Send'),
                               'class'=>'send',
                           ]);
                       }
                   }
                ]
            ],
            /*
            [
                 'class' => 'yii\grid\ActionColumn',
                 'contentOptions' => ['style' => 'width:260px;'],
                 'header'=>'Actions',
                 'template' => '{pick}{view}',
                 'buttons' => [

                    //pick button
                    'pick' => function ($url, $model) {
                        return Html::a("<span class='task-$model->id'></span>Pick", 'javascript:void(0)', [
                            'title' => Yii::t('app', 'Pick task'),
                            'class'=>'btn btn-primary btn-xs pick',
                        ]);
                    },
                     //view button
                     'view' => function ($url, $model) {
                         return Html::a("<span class='task-$model->id'></span>View", 'javascript:void(0)', [
                             'title' => Yii::t('app', 'View'),
                             'class'=>'btn btn-info btn-xs viewmail',
                         ]);
                     }
                 ]
              ],*/




            // 'emailID:email',
            // 'start',
            // 'end',
            // 'added',
            // 'updated',
            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

<div class="modalUpload modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <?php
                    echo FileInput::widget([
                        'name' => 'cvs',
                        'id' => 'input-id',
                        'pluginOptions' => [
                            'uploadUrl' => Url::to(['/newcvs/file-upload']),
                            'uploadExtraData' => [
                                // 'task_id' => Tasks::isProcessing()
                            ],
                            'showRemove' => false,
                            'maxFileCount' => 1
                            // 'allowedFileExtensions' => ['doc', 'docx', 'pdf', 'rtf'],
                        ]
                    ]);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modalSendmail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="sendmail" class="form-horizontal">
                    <fieldset>
                         <div class="form-group">
                            <label for="toEmail" class="col-lg-2 control-label">To</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="toEmail" id="toEmail" placeholder="To" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ccEmail" class="col-lg-2 control-label">Cc</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="ccEmail" id="ccEmail" placeholder="Cc">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="subjectEmail" class="col-lg-2 control-label">Subject</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="subjectEmail" id="subjectEmail" placeholder="Subject" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contentEmail" class="col-lg-2 control-label">Content</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="5" name="contentEmail" id="contentEmail"></textarea>
                            </div>
                        </div>
                        <div class="form-group modal-footer">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Send</button>
                            </div>
                        </div>
                        <input type="hidden" name="taskID" value="" />
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modalLoading">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="text-center">
                <img src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif" width="50%">
            </div>
        </div>
    </div>
</div>




<?php
$data = '';
if($gridData){
    $data = json_encode($gridData);
}
$this->registerJs("
    app = {
        gridData: $data,
        isProcessing: $isProcessing
    };
", \yii\web\View::POS_HEAD);
?>
