<?php

namespace ext\cvparser;

include 'simple_html_dom.php';

class SimpleDomUtil{
	public function __construct(){

	}

	/**
	 *
	 * Clean properties from style attribute in <tag>
	 * @param string $html
	 * @param tring $tag
	 * @param array $propeties
	 */
	public static function cleanStyleAttribute($html, $tag, $properties=array('position', 'top', 'left', 'right')){
		try{
			$html = str_get_html($html);
			foreach($html->find("[$tag style]") as $tag){
				$styles = $tag->getAttribute('style');
			 	$newStyle = "";
			 	if($styles){
					$styles = explode(';', $styles);
					foreach($styles as $style){
						$flag = true;
						for($i=0; $i < count($properties); $i++){
							$p = $properties[$i];
							$flag = $flag == (strpos($style, $p) === false);

						}
						if($flag){
							$newStyle .= $style.';';
						}
					}
					$tag->setAttribute('style', $newStyle);
			 	}
			}
		}catch(Exception $e){}

		return $html;
	}
}
