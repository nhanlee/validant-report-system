<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SourcingReport */

$this->title = 'Update Sourcing Report: ' . ' ' . $model->candidate;
$this->params['breadcrumbs'][] = ['label' => 'Sourcing Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->candidate, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sourcing-report-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
