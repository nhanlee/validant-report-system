<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\SourcingReport;
use app\models\Clients;
use app\models\SourcingReportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SourcingReportController implements the CRUD actions for SourcingReport model.
 */
class SourcingReportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','report'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SourcingReport models.
     * @return mixed
     */
    public function actionReport()
    {
        $searchModel = new SourcingReportSearch();
        $searchModel->report = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all SourcingReport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $post = Yii::$app->request->post();
        if($post){
            $candidate = $post['SourcingReport']['candidate'];
            $candidate = explode(',', $candidate);
            if($candidate){
                foreach ($candidate as $candidateName) {
                    $model = new SourcingReport();
                    $model->load($post);
                    $model->created_by = Yii::$app->user->getId();
                    $client = Clients::findOne($model->client_id);
                    $model->team = $client->team;
                    $model->candidate = $candidateName;
                    $model->save();
                }
                //Cache sourcer, recruiter and team
                $cache = Yii::$app->cache;
                $cache->set('sourcingInput', $post);

                Yii::$app->session->setFlash('inputFinish', '<strong>' . $post['SourcingReport']['candidate'] . '</strong> has been added successfully!');
                return $this->refresh();
            }
        } else {
            $model = new SourcingReport();
            //Get cache and load model
            $cache = Yii::$app->cache;
            $oldData = $cache->get('sourcingInput');
            if(isset($oldData['SourcingReport'])){
                if(isset($oldData['SourcingReport']['candidate'])){
                    $oldData['SourcingReport']['candidate'] = '';
                }

            }
            $model->load($oldData);

            $searchModel = new SourcingReportSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single SourcingReport model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SourcingReport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SourcingReport();
        if ($model->load(Yii::$app->request->post())) {
            $model->created_by = Yii::$app->user->getId();
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SourcingReport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $client = Clients::findOne($model->client_id);
            $model->team = $client->team;
            $model->updated_by = Yii::$app->user->getId();
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SourcingReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SourcingReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SourcingReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SourcingReport::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
