<?php

namespace app\controllers;

use Yii;
use app\models\Cvs;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Tasks;

/**
 * CvsController implements the CRUD actions for Cvs model.
 */
class CvsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cvs models.
     * @return mixed
     */
    public function actionFileUpload()
    {
        $post = Yii::$app->request->post();
        if (empty($_FILES['cvs']) || empty($post['task_id']) ) {
          echo json_encode(['error'=>'No files found for upload.']);
          return; // terminate
        }

        // get the files posted
        $file = $_FILES['cvs'];

        // a flag to see if everything is ok
        $success = false;

        // file paths to store
        $output = [];

        // get file names
        $task = Tasks::findOne($post['task_id']);
        
        $filenames = $file['name'];

        $target = Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . "cvs" . DIRECTORY_SEPARATOR . $task->email->getPrimaryKey() . '_v1_' . $filenames;
        
        if(move_uploaded_file($file['tmp_name'], $target)) {
            $success = true;
        } else {
            $success = false;
        }

        if ($success === true) {
          $newcvs = new Cvs();
          $newcvs->name = $filenames;
          $newcvs->emailID = $task->emailID;
          $newcvs->taskID = $post['task_id'];
          $newcvs->save();
        } elseif ($success === false) {
          $output = ['error'=>'Error while uploading file. Contact the system administrator'];
          unlink($target);
        } else {
          $output = ['error'=>'No files were processed.'];
        }

        // return a json encoded response for plugin to process successfully
        echo json_encode($output);

    }

    protected function findModel($id)
    {
        if (($model = Cvs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
