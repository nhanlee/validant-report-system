<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="notifications"></div>
<div class="template-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Template', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'type',
            // 'subject',
            // 'content:html',
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'attribute' => 'Type',
                'contentOptions' =>['class' => 'type'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->type;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'attribute' => 'Subject',
                'contentOptions' =>['class' => 'subject'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->subject;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'attribute' => 'Content',
                'contentOptions' =>['class' => 'content'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->content;
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' =>['width' => '6%'],
                'contentOptions' =>['class' => 'actions'],
                'template' => '{view} {update} {send}',
                'buttons' => [
                   //send button
                   'send' => function ($url, $model) {
                       if(Yii::$app->user->identity->group == 1){
                           return Html::a("<span class='glyphicon glyphicon-send'></span>", 'javascript:void(0)', [
                               'title' => Yii::t('app', 'Send'),
                               'class'=>'sendTemplate',
                           ]);
                       }
                   }
                ]
            ],
        ],
    ]); ?>

</div>

<div class="modal modalSendmail">
    <div class="modal-dialog" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="sendmailTemplate" class="form-horizontal">
                    <fieldset>
                         <div class="form-group">
                            <label for="toEmail" class="col-lg-2 control-label">To</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="toEmail" id="toEmail" placeholder="To" required
                                value='ckwong@validant.com, rkainth@validant.com, pchekuri@validant.com, cmoncrief@validant.com, gabdulrahim@validant.com'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ccEmail" class="col-lg-2 control-label">Cc</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="ccEmail" id="ccEmail" placeholder="Cc">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subjectEmail" class="col-lg-2 control-label">Subject</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="subjectEmail" id="subjectEmail" placeholder="Subject" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contentEmail" class="col-lg-2 control-label">Content</label>
                            <div class="col-lg-10">
                                <?php
                                    echo CKEditor::widget([
                                        'name' => 'contentEmail',
                                        'options' => ['rows' => 6],
                                        'preset' => 'standard'
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group modal-footer">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Send</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalLoading">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="text-center">
                <img src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif" width="50%">
            </div>
        </div>
    </div>
</div>