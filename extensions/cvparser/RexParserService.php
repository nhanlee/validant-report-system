<?php

namespace ext\cvparser;

/**
 * Implements Resume parser using SimpleRexService.
 *
 * Need include simplehtmldom extension to clear style attribute
 *
 * @link http://75.101.132.136:3333/wsdl/ISimpleRexService
 * @since sprint25
 */

class RexParserService extends AbstractService
{

    protected $_wsdl = 'http://75.101.132.136:3333/wsdl/ISimpleRexService';

    public function __construct($wsdl = null)
    {
        if ($wsdl) {
            $this->setWsdl($wsdl);
        }
    }

    /**
     * Set WSDL of SimpleRexService for SOAP calls
     *
     * @param string $wsdl
     */
    public function setWsdl($wsdl)
    {
        $this->_wsdl = $wsdl;
    }

    /**
     * Get WSDL of SimpleRexService for SOAP calls
     *
     * @return string
     */
    public function getWsdl()
    {
        return $this->_wsdl;
    }

    /**
     * Parse CV from given string
     *
     * @param string $content
     * @return boolean
     * @throws Exception throw exception if has failures
     */
    public function parse($content)
    {
        try {
            $soap = new \SoapClient($this->getWsdl());
            $res = $soap->ProcessResume($content, false, true, false);

            if ($res->RexStatus != 0) {
                throw new Exception('REX error: ' . $res->RexStatusStr);
            }

            # INFO: fix for broken REX's namespaces
            # FIXME: consider:
            #       moving to separate class ?
            # 	$namespaces = $xml->getNamespaces(true);
            # 	$xml->registerXPathNamespace("default", current($namespaces));
            $xml = iconv('UTF-16LE', 'UTF-8', $res->Xml);
            $xml = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $xml);
            $xml = preg_replace('/<Resume.* xml:lang="en">/', '<Resume xmlns:rm="resumemirror.com" xmlns:hr="http://ns.hr-xml.org/PersonDescriptors" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xml:lang="en">', $xml);
            $xml = new \SimpleXMLElement($xml);

            $result = array();

            # -----  Basic information
            $result['basic']['name'] = (string) current($xml->xpath('//PersonName/GivenName'));
            $result['basic']['surname'] = (string) current($xml->xpath('//PersonName/FamilyName'));
            $result['basic']['email'] = (string) current($xml->xpath('//ContactMethod/InternetEmailAddress'));
            $result['basic']['country'] = (string) current($xml->xpath('//PostalAddress/CountryCode'));
            $result['basic']['highestDegree'] = (string) current($xml->xpath('//rm:HighestDegree'));

            # -----  Address information
            $result['address']['address1'] = (string) current($xml->xpath('//PostalAddress/DeliveryAddress/AddressLine'));

            //VSRIIV-4073: The new Quick Preview interface is bad -  Format phone number
            $result['address']['homephone'] = (string) current($xml->xpath('//Telephone/InternationalCountryCode')) ? '+' . current($xml->xpath('//Telephone/InternationalCountryCode')) : '';
            $result['address']['homephone'] .= (string) current($xml->xpath('//Telephone/AreaCityCode')) ? '-' . current($xml->xpath('//Telephone/AreaCityCode')) : '';
            $result['address']['homephone'] .= (string) current($xml->xpath('//Telephone/SubscriberNumber')) ? '-' . mb_substr(preg_replace('/\s|[^\d\+]/', '', current($xml->xpath('//Telephone/SubscriberNumber'))), 0, 20) : '';

            $result['address']['mobile'] = (string) current($xml->xpath('//Mobile/InternationalCountryCode')) ? '+' . current($xml->xpath('//Mobile/InternationalCountryCode')) : '';
            $result['address']['mobile'] .= (string) current($xml->xpath('//Mobile/AreaCityCode')) ? '-' . current($xml->xpath('//Mobile/AreaCityCode')) : '';
            $result['address']['mobile'] .= (string) current($xml->xpath('//Mobile/SubscriberNumber')) ? '-' . mb_substr(preg_replace('/\s|[^\d\+]/', '', current($xml->xpath('//Mobile/SubscriberNumber'))), 0, 20) : '';

            // remove all chars except of digits. workaround for BH 20 char limitation
            //$result['address']['homephone'] = mb_substr(preg_replace('/\s|[^\d\+]/', '', $result['address']['homephone']), 0, 20);

            // $result['address']['mobile'] = (string) current($xml->xpath('//Mobile/InternationalCountryCode')) . ' ' .
            //         (string) current($xml->xpath('//Mobile/AreaCityCode')) . ' ' .
            //         (string) current($xml->xpath('//Mobile/SubscriberNumber'));

            // remove all chars except of digits. workaround for BH 20 char limitation
            //$result['address']['mobile'] = mb_substr(preg_replace('/\s|[^\d\+]/', '', $result['address']['mobile']), 0, 20);

            $result['address']['country'] = (string) current($xml->xpath('//PostalAddress/CountryCode'));
            $result['address']['region'] = (string) current($xml->xpath('//PostalAddress/Region'));
            $result['address']['municipality'] = (string) current($xml->xpath('//PostalAddress/Municipality'));
            $result['address']['postal'] = (string) current($xml->xpath('//PostalAddress/PostalCode'));

            # ----- Education history
            $result['edu'] = array();
            foreach ($xml->xpath("//EducationHistory/SchoolOrInstitution") as $edu) {
                $e = array();
                $e['school_type'] = (string) $edu->attributes()->schoolType;
                $e['school_name'] = (string) current($edu->School->SchoolName);
                $e['municipality'] = (string) current($edu->LocationSummary->Municipality);
                $e['region'] = (string) current($edu->LocationSummary->Region);
                $e['country'] = (string) current($edu->LocationSummary->CountryCode);
                $e['degree_name'] = (string) current($edu->Degree->DegreeName);
                $e['degree_major'] = (string) current($edu->Degree->DegreeMajor->Name);
                $e['start_year'] = (string) current($edu->Degree->DatesOfAttendance->StartDate);
                $e['end_year'] = (string) current($edu->Degree->DatesOfAttendance->EndDate);

                $result['edu'][] = $e;
            }

            # -----  Work Experience
            $result['work'] = array();
            foreach ($xml->xpath("//EmploymentHistory/EmployerOrg") as $work) {
                $w = array();
                $w['employer'] = (string) current($work->EmployerOrgName);
                $w['title'] = (string) current($work->PositionHistory->Title);
                $w['summarytext'] = (string) current($work->PositionHistory->Description);
                $w['startdate'] = (string) current($work->PositionHistory->StartDate);
                $w['enddate'] = (string) current($work->PositionHistory->EndDate);
                $result['work'][] = $w;
            }

            # -----  Details
            $result['detail']['summary'] = (string) current($xml->xpath('//rm:Experience[rm:ExperienceKind="Summary"]/rm:Detail'));
            $result['detail']['highestindustry'] = (string) current($xml->xpath('//rm:Experience[rm:ExperienceKind="HighestIndustry"]/rm:Detail'));
            $result['detail']['management'] = (string) current($xml->xpath('//rm:Experience[rm:ExperienceKind="Management"]/rm:Detail'));
            $result['detail']['topskills'] = (string) current($xml->xpath('//rm:Experience[rm:ExperienceKind="TopSkills"]/rm:Detail'));
            $result['detail']['toplevel'] = (string) current($xml->xpath('//rm:Experience[rm:ExperienceKind="TopLevel"]/rm:Detail'));

            # -----  Html
            $html = iconv('UTF-16LE', 'UTF-8', $res->ResumeHtml);

            try{
            	$html = SimpleDomUtil::cleanStyleAttribute($html, 'div');
            }catch(Exception $e){}

            //encode base64 for can store in hidden tag field
            $result['html'] = base64_encode(bzcompress($html));

            $this->_result = $result;

            return true;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    /**
     * Parse CV from given string
     *
     * @param string $content
     * @return boolean
     * @throws Exception throw exception if has failures
     */
    public function shortParse($content)
    {
        try {
            $soap = new \SoapClient($this->getWsdl());
            $res = $soap->ProcessResume($content, false, true, false);

            if ($res->RexStatus != 0) {
                return false;
                throw new Exception('REX error: ' . $res->RexStatusStr);
            }

            # INFO: fix for broken REX's namespaces
            # FIXME: consider:
            #       moving to separate class ?
            # 	$namespaces = $xml->getNamespaces(true);
            # 	$xml->registerXPathNamespace("default", current($namespaces));
            $xml = iconv('UTF-16LE', 'UTF-8', $res->Xml);
            $xml = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $xml);
            $xml = preg_replace('/<Resume.* xml:lang="en">/', '<Resume xmlns:rm="resumemirror.com" xmlns:hr="http://ns.hr-xml.org/PersonDescriptors" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xml:lang="en">', $xml);
            $xml = new \SimpleXMLElement($xml);

            $result = array();

            # -----  Basic information
            $result['basic']['name'] = (string) current($xml->xpath('//PersonName/GivenName'));
            $result['basic']['surname'] = (string) current($xml->xpath('//PersonName/FamilyName'));

            $this->_result = $result;

            return true;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
}
