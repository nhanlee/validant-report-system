<?php

namespace ext\cvparser;

/**
 * Abstract Parser Service
 */
abstract class AbstractService implements IParserService
{

    protected $_result = array();

    public function getResult()
    {
        return $this->_result;
    }
}
