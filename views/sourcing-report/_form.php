<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sourcer;
use app\models\Clients;
use app\models\SourcingType;
use dosamigos\selectize\SelectizeDropDownList;
use dosamigos\selectize\SelectizeTextInput;

/* @var $this yii\web\View */
/* @var $model app\models\SourcingReport */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $team = Yii::$app->params['team']; ?>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'type')->widget(SelectizeDropDownList::classname(),[
                'items' => ArrayHelper::map(SourcingType::find()->where(['type' => SourcingType::TYPE_SOURCING])
                                                                ->orderBy('name')
                                                                ->all(), 'id', 'name'),
                'clientOptions' => [
                    'create' => true,
                ],
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'sourcer_id')->dropDownList(
                                                        ArrayHelper::map(Sourcer::find()->where(['sourcer_active' => Sourcer::ACTIVE])
                                                                                        ->orderBy('sourcer_name, sourcer_surname')
                                                                                        ->all(), 'sourcer_id', 'Text'),
                                                        ['prompt' => '--Choose a sourcer--']
                                                    ) ?>
        </div>
        <div class="col-md-2">
            <?php
                $clients = ArrayHelper::map(Clients::find()->where(['client_active' => Clients::ACTIVE,'team' =>[0,1,2,3]])
                                                           ->orderBy('client_name')
                                                           ->all(), 'client_id', 'client_name', 'team');
                ksort($clients);
                unset($team[4]);
                unset($team[5]);
                $items = array_combine($team, array_values($clients));
            ?>
            <?= $form->field($model, 'client_id')->dropDownList(
                                                        $items,
                                                        ['prompt' => '--Choose a recruiter--']
                                                    ) ?>
        </div>
        <div class="col-md-3">
            <?php if(Yii::$app->controller->action->id == 'update'): ?>
                <?= $form->field($model, 'candidate')->textInput() ?>
            <?php else: ?>
                <?= $form->field($model, 'candidate')->widget(SelectizeTextInput::classname(),[
                    'name' => 'tags',
                    'clientOptions' => [
                        'plugins' => ['remove_button'],
                        'create' => true
                    ],
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'note')->textInput() ?>
        </div>
    </div>

    <div class="text-right">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

