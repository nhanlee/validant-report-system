<?php

namespace app\controllers;

use Yii;
use app\models\Tasks;
use app\models\TasksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Email;
use app\models\SourcingReport;
use app\models\Clients;
/**
 * TasksController implements the CRUD actions for Tasks model.
 */
class TasksController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tasks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TasksSearch();
        $searchModel->report = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $isProcessing = Tasks::isProcessing();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'isProcessing' => $isProcessing,
        ]);
    }

    public function actionViewContent($email = null)
    {
        if($email){
            $mail = Email::findOne($email);
            if($mail){
                $content = bzdecompress(base64_decode($mail->content));
                if(!$content){
                    $content = bzdecompress(base64_decode($mail->text));
                }
                return $this->render('view-content', [
                    'content' => $content,
                ]);
            }
        }
    }

    public function actionDone()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //pick ok or not by sourcer
        $msg = '';
        $statusEl = '';
        $success = false;
        if(isset($_POST['taskID'])){
            $taskID = $_POST['taskID'];
            $task = $this->findModel($taskID);
            if($task){
                $userID = $task->user;
                if($userID == Yii::$app->user->getId() && $task->status == Tasks::STATUS_UPLOADED){
                    //ok, you can set it done
                    $task->status = Tasks::STATUS_DONE;
                    $task->end = date("Y-m-d H:i:s");
                    if($task->save()){
                        //Automatically create sourcing report
                        $client = Clients::find()->where("client_name like '%$task->recruiter%'")->one();
                        if(!$client){
                            //Create new client
                            $client = new Clients();
                            $client->client_name = $task->recruiter;
                            $client->team =  0;//EU
                            $client->save();
                        }
                        $sourcer_id = null;
                        if(Yii::$app->user->identity->sourcer){
                            $sourcer_id = Yii::$app->user->identity->sourcer->sourcer_id;
                        }
                        $sreport = new SourcingReport();
                        $sreport->type = 1;//Resume Formatting
                        $sreport->sourcer_id = $sourcer_id ? $sourcer_id : Yii::$app->user->getId();
                        $sreport->client_id = $client->getPrimaryKey();
                        $sreport->team = $client->team;
                        $sreport->created_by = Yii::$app->user->getId();
                        $sreport->candidate = $task->candidate;
                        $sreport->note = '';
                        $sreport->save();
                        $msg = 'Well done, You have finished task!';
                        $success = true;
                    }
                } else {
                    $msg = "Oops, Please contact Administrator, thanks!";
                    $success = false;
                }
                $statusEl = $task->getHtmlStatus();
            }
        }

        $data = [
            'success'   => $success,
            'data'      => [
                'msg' =>$msg,
                'statusEl' =>$statusEl,
            ]

        ];
        return $data;
    }
    /**
     * Lists all Tasks models.
     * @return mixed
     */
    public function actionPick()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //pick ok or not by sourcer
        $userName = '';
        $statusEl = '';
        if(isset($_POST['taskID'])){
            $taskID = $_POST['taskID'];
            $task = $this->findModel($taskID);
            if($task){
                $userID = $task->user;
                if(!$userID){
                    //ok, you can pick this task
                    $task->user = Yii::$app->user->getId();
                    $task->start = date("Y-m-d H:i:s");
                    $task->status = TASKS::STATUS_PROCESSING;
                    $task->save();
                    $userID = Yii::$app->user->getId();
                    $userName = 'You';
                    $msg = 'Well done, <strong>You</strong> have picked this task';
                } else {
                    $user = User::findIdentity($userID);
                    $userName = $user->username;
                    $msg = "Well done, <strong>$userName</strong> has picked this task";
                    if($user->id == Yii::$app->user->getId()){
                        $msg = 'Well done, <strong>You</strong> have picked this task';
                    }
                }
                $statusEl = $task->getHtmlStatus();
            }
        }

        $data = [
            'success'   => ($userID == Yii::$app->user->getId()) ? true : false,
            'data'      => [
                'taskID' =>$taskID,
                'userID' =>$userID,
                'userName' =>$userName,
                'msg' =>$msg,
                'statusEl' =>$statusEl,
            ]

        ];
        return $data;
    }

    /**
     * Displays a single Tasks model.
     * @param integer $id
     * @return mixed
     */
    public function actionSend()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //pick ok or not by sourcer
        $msg = '';
        $statusEl = '';
        $success = false;
        if(isset($_POST['taskID'])){
            $taskID = $_POST['taskID'];
            $task = $this->findModel($taskID);
            if($task){
                $userID = $task->user;
                if(($userID == Yii::$app->user->getId() && $task->status == Tasks::STATUS_DONE) || Yii::$app->user->identity->group == 1){
                    //ok, you can send email for this task
                    $cc = array();
                    $to = array();
                    $newcvs = $task->newcvs; //Get file uploaded
                    if(isset($_POST['toEmail']) && $_POST['toEmail'] != ''){
                        $to = explode(',', $_POST['toEmail']);//array to
                        $to = array_map('trim', $to);
                    }
                    if(isset($_POST['ccEmail']) && $_POST['ccEmail'] != ''){
                        $cc = explode(',', $_POST['ccEmail']);//array cc
                        $cc = array_map('trim', $cc);
                    }
                    if(YII_DEBUG){
                        $to = array(Yii::$app->params['adminEmail']);
                        $cc = array(Yii::$app->params['adminEmail']);
                    }
                    $message = Yii::$app->mailer->compose();
                    $message->attach(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . '/newcvs/' . $newcvs->name);
                    if(count($cc) > 0){
                        $message->setCc($cc);
                    }
                    $t = $message->setFrom(Yii::$app->params['imap']['account'])
                            ->setTo($to)
                            ->setSubject($_POST['subjectEmail'])
                            ->setTextBody('Plain text content')
                            ->setHtmlBody($_POST['contentEmail'])
                            ->send();
                    if(Yii::$app->user->identity->group != 1){
                        $task->status = Tasks::STATUS_SENT;
                        $task->save();
                    }
                    $msg = 'Well done, You have sent email for this task!';
                    $success = true;
                } else {
                    $msg = "Oops, Please contact Administrator, thanks!";
                    $success = false;
                }
                $statusEl = $task->getHtmlStatus();
            }
        }

        $data = [
            'success'   => $success,
            'data'      => [
                'msg' =>$msg,
                'statusEl' =>$statusEl,
                'taskID' =>$_POST['taskID'],
            ]

        ];
        return $data;
    }

    /**
     * Displays a single Tasks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tasks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tasks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Tasks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tasks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tasks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tasks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tasks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
