<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email".
 *
 * @property integer $id
 * @property string $date
 * @property string $subject
 * @property string $recruiter
 * @property string $content
 * @property string $text
 * @property integer $status
 * @property integer $number
 * @property string $added
 * @property string $updated
 */
class Email extends \yii\db\ActiveRecord
{
    const STATUS_NOTPARSE = 0;
    const STATUS_PARSED = 1;
    const STATUS_FALSE = 3;

    public function getCvs()
    {
        return $this->hasMany(Cvs::className(), ['emailID' => 'id']);
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'added', 'updated'], 'safe'],
            [['content'], 'string'],
            [['text'], 'string'],
            [['status', 'number'], 'integer'],
            [['subject', 'recruiter'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'subject' => 'Subject',
            'recruiter' => 'Recruiter',
            'content' => 'Content',
            'text' => 'Text',
            'status' => 'Status',
            'number' => 'Number',
            'added' => 'Added',
            'updated' => 'Updated',
        ];
    }
}
