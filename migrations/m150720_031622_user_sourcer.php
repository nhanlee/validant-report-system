<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_031622_user_sourcer extends Migration
{
    public function up()
    {
        $sql="ALTER TABLE `user` ADD COLUMN `sourcer_id` INT(11) UNSIGNED NULL AFTER `updated_at`;";
        $this->execute($sql);
        echo "Alter done" .'\n';
    }

    public function down()
    {
        echo "m150720_031622_user_sourcer cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
