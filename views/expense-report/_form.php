<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\SourcingType;
use app\models\Clients;
use app\models\Sourcer;
use dosamigos\selectize\SelectizeDropDownList;

/* @var $this yii\web\View */
/* @var $model app\models\ExpenseReport */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $team = Yii::$app->params['team']; ?>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'type')->widget(SelectizeDropDownList::classname(),[
                'items' => ArrayHelper::map(SourcingType::find()->where(['type' => SourcingType::TYPE_EXPENSE])
                                                                ->orderBy('name')
                                                                ->all(), 'id', 'name'),
                'clientOptions' => [
                    'create' => true,
                ],
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'sourcer_id')->dropDownList(
                                                        ArrayHelper::map(Sourcer::find()->where(['sourcer_active' => Sourcer::ACTIVE])
                                                                                        ->orderBy('sourcer_name, sourcer_surname')
                                                                                        ->all(), 'sourcer_id', 'Text'),
                                                        ['prompt' => '--Choose a sourcer--']
                                                    ) ?>
        </div>
        <div class="col-md-2">
            <?php
                $clients = ArrayHelper::map(Clients::find()->where(['client_active' => Clients::ACTIVE,'team' => [4,5]])
                                                           ->orderBy('client_name')
                                                           ->all(), 'client_id', 'client_name', 'team');
                ksort($clients);
                unset($team[0]);
                unset($team[1]);
                unset($team[2]);
                unset($team[3]);
                $items = array_combine($team, array_values($clients));
            ?>
            <?= $form->field($model, 'client_id')->dropDownList(
                                                        $items,
                                                        ['prompt' => '--Choose a Requested By--']
                                                    ) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'beginning')->textInput() ?>
        </div>
        <div class="col-md-2">
             <?= $form->field($model, 'done')->textInput() ?>
        </div>
        <div class="col-md-2">
             <?= $form->field($model, 'old')->textInput() ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'note')->textInput() ?>
        </div>
    </div>

    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

