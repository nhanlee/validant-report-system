<?php

use yii\db\Schema;
use yii\db\Migration;

class m150826_054122_old_expense extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `expense_report` 
                ADD COLUMN `old` INT(10) UNSIGNED NOT NULL AFTER `done`;";
        $this->execute($sql);
        echo "Done Old" .'\n';
    }

    public function down()
    {
        echo "m150826_054122_old_expense cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
