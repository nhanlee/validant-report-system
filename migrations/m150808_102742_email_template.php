<?php

use yii\db\Schema;
use yii\db\Migration;

class m150808_102742_email_template extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE `template` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NULL,
  `subject` VARCHAR(45) NULL,
  `content` TEXT NULL,
  `added` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));";
        $this->execute($sql);

    }

    public function down()
    {
        echo "m150808_102742_email_template cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
