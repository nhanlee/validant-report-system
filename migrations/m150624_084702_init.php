<?php

use yii\db\Schema;
use yii\db\Migration;

class m150624_084702_init extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE `clients` (
              `client_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `client_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
              `client_active` tinyint(1) unsigned DEFAULT '1',
              `client_added` int(10) unsigned NOT NULL,
              `client_updated` int(10) NOT NULL,
              `team` tinyint(3) DEFAULT NULL,
              PRIMARY KEY (`client_id`)
            ) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $this->execute($sql);
        echo "Done Client" .'\n';

        $sql = "INSERT INTO `clients` VALUES (80,'Emily Warner','ewarner@validant.com',1,1202958791,1421696191,2),(17,'Jonathan Moore','',1,1197935257,0,3),(34,'Maya Kraft','maya@apariant.com',1,1201144017,0,4),(1031,'Aaron Culig','aculig@validant.com',1,1246445642,1433454699,4),(127,'Helm Siegel','hsiegel@validant.com',1,1205431173,0,4),(239,'Colin Larsen','clarsen@validant.com',1,1216664272,0,4),(931,'Paul Huish','phuish@validant.com',1,1238502543,0,4),(935,'John Tritschler','jtritschler@validant.com',1,1238502833,0,4),(937,'Theodore Hellen','thellen@validant.com',1,1238502933,0,4),(1200,'Matt Valentino','mvalentino@validant.com',1,1264635524,0,4),(1346,'Dean Molloy','dmolloy@validant.com',1,1274256918,0,4),(1712,'Katie Phillips','kphillips@validant.com',1,1308345289,1421108019,1),(1760,'Anh Ho','anh@validant.com',1,1312421261,0,4),(1902,'Jennifer Holy','jholy@validant.com',1,1321885498,0,4),(2008,'Alexa Tralla','atralla@validant.com',1,1328852526,0,0),(2052,'Hien Luong','hien@vsource.net',1,1332905456,1416884717,4),(2080,'Heather Keller','hkeller@validant.com',1,1334797527,1418936692,4),(2082,'Mara Yanoshik','myanoshik@validant.com',1,1334797565,0,4),(2120,'Michael Bracey','mbracey@validant.com',1,1336528012,0,4),(2122,'Marko Stimpfel','mstimpfel@validant.com',1,1336528035,1420558904,3),(2174,'Van Dinh','vdinh@validant.com',1,1339408304,0,3),(2472,'Mike Spence','mspence@validant.com',1,1352940580,0,3),(2474,'Jimmy Varga','Jvarga@validant.com',1,1352940611,1402490031,3),(2584,'Margaretta Sweezy','msweezy@validant.com',1,1357088903,0,1),(2720,'George Hong','ghong@validant.com',1,1360113227,1410811829,1),(2721,'Josh Fishman','jfishman@validant.com',1,1360113322,0,4),(2824,'Ashley Ahwal','aahwal@validant.com',1,1367472814,1412181656,4),(2825,'Joe Caron','jcaron@validant.com',1,1367472909,1432142617,4),(2826,'Joseph Luke','jluke@validant.com',1,1367472934,0,4),(2828,'Ryan Antisdale','rantisdale@validant.com',1,1367472973,0,4),(3088,'Jon Lapham','jlapham@validant.com',1,1386646023,0,4),(2938,'Jonathon Moore','jmoore@validant.com',1,1375800559,0,4),(2977,'Janice Leung','jleung@validant.com',1,1379471909,0,1),(2978,'Jason Chu','jchu@validant.com',1,1379471951,1424372577,2),(3013,'Dan Merrill','dmerrill@validant.com',1,1380678195,1430137145,3),(3021,'Stephanie Colotti','scolotti@validant.com',1,1381211241,0,4),(3152,'vsource support','support_validant@vsource.net',1,1392357009,1428042141,4),(3221,'Phil Cahill','pcahill@validant.com',1,1396321937,1421063625,0),(3288,'Jonathan Wong','jwong@validant.com',1,1401327066,1401837317,2);";
        $this->execute($sql);
        echo "Done Insert Client" .'\n';

        $sql = "CREATE TABLE `user` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
                  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `status` smallint(6) NOT NULL DEFAULT '10',
                  `created_at` int(11) NOT NULL,
                  `updated_at` int(11) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $this->execute($sql);
        echo "Done user" .'\n';

        $sql = "CREATE TABLE `sourcing_type` (
                  `id` int(10) NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) NOT NULL,
                  `type` tinyint(3) DEFAULT '1',
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->execute($sql);
        echo "Done sourcing_type" .'\n';

        $sql = "CREATE TABLE `sourcing_report` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `type` tinyint(5) unsigned NOT NULL DEFAULT '1',
                  `sourcer_id` int(11) NOT NULL,
                  `client_id` int(11) NOT NULL,
                  `team` int(11) NOT NULL,
                  `candidate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `created_by` int(3) unsigned DEFAULT NULL,
                  `added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $this->execute($sql);
        echo "Done sourcing_report" .'\n';

        $sql = "CREATE TABLE `sourcer` (
              `sourcer_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
              `sourcer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `sourcer_surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `sourcer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `sourcer_active` tinyint(3) unsigned DEFAULT '1',
              `sourcer_added` int(10) NOT NULL,
              `sourcer_updated` int(10) NOT NULL,
              PRIMARY KEY (`sourcer_id`)
            ) ENGINE=MyISAM AUTO_INCREMENT=976 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $this->execute($sql);
        echo "Done sourcer" .'\n';

        $sql = "INSERT INTO `sourcer` VALUES (294,'Ty','Doan','ty.doan@vsource.io',1,1199158715,0),(558,'Hanh','Dam','hanh.dam@vsource.io',1,1300849394,0),(588,'Tuyet','Huynh','tuyet@vsource.net',1,1306888609,0),(660,'Thi','Huynh','thi.huynh@vsource.io',1,1318813032,0),(746,'Anh','Nguyen Thi Tuyet','anh.nguyen2@vsource.io',1,1373004274,0),(764,'Trinh Diem','Nguyen','trinh.nguyen@vsource.io',1,1378687974,0),(798,'Duong','Tran','duongtran@kinsale.vn',1,1388452216,0),(776,'Nhung','Le','nhung.le@vsource.io',1,1381386967,0),(781,'Linh','Ho','linh.ho@vsource.io',1,1381991078,0),(797,'Anh','Quy','anh.le@vsource.io',1,1388452090,0),(808,'Nhan','Pham','nhan.pham@vsource.io',1,1390618500,0),(815,'Thoa','Tran','thoa.tran@vsource.io',1,1392282856,0),(819,'Phung','Tran','phung.tran@vsource.io',1,1392888085,0),(852,'Hang','Vu','hang.vu@vsource.io',1,1397785966,0),(884,'Hang','Nguyen Ngoc','hang.nguyen2@vsource.io',1,1402880233,0),(862,'Vu','Le','vu.le@vsource.io',1,1399019188,0),(916,'Khoa','Do','khoa.do@vsource.io',1,1408063526,0),(913,'Khoi','Hoang','khoi.hoang@vsource.io',1,1406258213,0),(927,'Au','Nguyen','aunguyen@kinsale.vn',1,1413946286,0),(933,'Thao','Pham','thao.pham@vsource.io',1,1414475073,0),(928,'Huy','Nguyen','huy.nguyen2@vsource.io',1,1413946320,0),(946,'Duc Huy','Nguyen','huy.nguyen3@vsource.io',1,1421030091,0),(975,'Truong','Nguyen','truong.nguyen2@vsource.io',1,1433466119,0),(944,'Thong','Le','thong.le@vsource.io',1,1420782120,0),(951,'Huyen','Vu','huyen.vu@vsource.io',1,1426054010,0),(952,'Tien','Nguyen','tien.nguyen@vsource.io',1,1426122939,0),(958,'Phuong','Vo','phuong.vo@vsource.io',1,1426741739,0),(959,'Lam','Tran','lam.tran@vsource.io',1,1426741773,0),(966,'Trang','Vu','trang.vu@vsource.io',1,1432009261,0),(974,'Hanh','Huynh','hanh.huynh@vsource.io',1,1432776602,0);";
        $this->execute($sql);
        echo "Done Insert Sourcer" .'\n';

        $sql = "CREATE TABLE `expense_report` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `type` tinyint(3) unsigned NOT NULL DEFAULT '1',
                  `sourcer_id` int(11) NOT NULL,
                  `client_id` int(11) NOT NULL,
                  `team` int(11) NOT NULL,
                  `beginning` int(10) unsigned NOT NULL DEFAULT '0',
                  `done` int(10) unsigned NOT NULL DEFAULT '0',
                  `created_by` int(3) unsigned DEFAULT NULL,
                  `added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $this->execute($sql);
        echo "Done expense_report" .'\n';

    }

    public function down()
    {
        echo "m150624_084702_init cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
