<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Sourcer;
use app\models\Clients;
use app\models\Tasks;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SourcingReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report';
$this->params['breadcrumbs'][] = $this->title;
$dateRange = null;
if(isset(Yii::$app->request->queryParams['dateRange'])){
  $dateRange = Yii::$app->request->queryParams['dateRange'];
}

$averageTime = Tasks::averageTime($dateRange);
$quanlity = Tasks::Quanlity($dateRange);
$RequestByRecruiters = Tasks::RequestByRecruiters($dateRange);
$flag1 = '';
$flag2 = '';
$fade1 = '';
$fade2 = '';
if(isset(Yii::$app->request->queryParams['ValidantReportSearch'])){
  $flag1 = '';
  $flag2 = 'active';
  $fade1 = '';
  $fade2 = 'active in';
} else {
  $flag1 = 'active';
  $flag2 = '';
  $fade1 = 'active in';
  $fade2 = '';
}
?>

<ul class="nav nav-tabs" style="margin-bottom: 15px;">
    <li class="<?= $flag1?>"><a href="#formatting" data-toggle="tab" id= 'test2'>FORMATTING RESUME REPORT</a></li>
    <li class="<?= $flag2?>" ><a href="#generate" data-toggle="tab" id= 'test1'>GENERATE REPORT</a></li>
</ul>
<div id="myTabContent" class="tab-content">
    <div class="tab-pane fade <?= $fade1?>" id="formatting">
      <?php  echo $this->render('_chart', ['dateRange' => $dateRange]); ?>
      <div id="columnChart"></div>
      <div id="stackChart"></div>
      <div id="barChart"></div>
    </div>
    <div class="tab-pane fade <?= $fade2?>" id="generate">

        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php if(isset(Yii::$app->request->queryParams['ValidantReportSearch']['added'])): ?>
        <?php if(Yii::$app->request->queryParams['ValidantReportSearch']['added'] != ''): ?>
              <div class="text-right">
                <?= Html::a('Export to PDF', ['export', 'data' => Yii::$app->request->queryParams], ['class' => 'btn btn-info']) ?>
              </div>

              <div class="validant-report">
                <?php if($data['part1']): ?>
                  <table class="table table-striped table-bordered">
                    <caption><strong>PART 1: SUMMARY</strong></caption>
                    <thead>
                      <tr>
                        <th style="width: 45px;text-align: center;">No</th>
                        <th>Description</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data['part1'] as $index => $d):?>
                        <tr>
                          <td style="text-align: center;"><?= $index+1; ?></td>
                          <td><?= $d['des']; ?></td>
                          <td><?= $d['total']; ?></td>
                        </tr>
                      <?php endforeach;?>
                    </tbody>
                  </table>
                <?php endif; ?>

                <?php if($data['part2']): ?>
                  <table class="table table-striped table-bordered">
                    <caption><strong>PART 2: REPORT ELLABORATION</strong></caption>
                    <thead>
                      <tr>
                        <th style="width: 45px; text-align: center;">No</th>
                        <th style="text-align:center;">Team</th>
                        <th>Job</th>
                        <th style="width: 115px">New Resume</th>
                        <th style="width: 115px">Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $grandTotalNew = 0;
                        $grandTotalTotal = 0;
                      ?>
                      <?php foreach($data['part2'] as $key => $team):?>
                        <?php
                          $totalNew = 0;
                          $totalTotal = 0;
                        ?>
                        <?php foreach($team as $index => $job):?>
                            <tr>
                              <td style="text-align: center;"><?= $index+1; ?></td>
                                <?php if($index == 0):?>
                                  <td style="vertical-align: middle; text-align:center; background-color: #ffffff;" rowspan="<?= count($team); ?>"><?= $key; ?></td>
                                <?php endif;?>
                              <td><?= $job['job_title']; ?></td>
                              <td><?= $job['new']; ?></td>
                              <td><?= $job['total']; ?></td>
                            </tr>
                            <?php
                              $totalNew += $job['new'];
                              $totalTotal += $job['total'];
                            ?>
                        <?php endforeach;?>
                            <tr style="  background-color: #a7be9c;">
                              <td colspan="3" style="text-align: center;">TOTAL</td>
                              <td><?= $totalNew; ?></td>
                              <td><?= $totalTotal; ?></td>
                            </tr>
                        <?php
                          $grandTotalNew += $totalNew;
                          $grandTotalTotal += $totalTotal;
                        ?>
                      <?php endforeach;?>
                            <tr style="  background-color: #a779a2;">
                              <td colspan="3" style="text-align: center;">GRAND TOTAL</td>
                              <td><?= $grandTotalNew; ?></td>
                              <td><?= $grandTotalTotal; ?></td>
                            </tr>
                    </tbody>
                  </table>
                <?php endif; ?>

                <?php if($sourcingProvider): ?>
                  <?= GridView::widget([
                      'dataProvider' => $sourcingProvider,
                      'summary' =>'',
                      'caption' =>'<strong>PART 3: ADMIN TASKS</strong>',
                      'columns' => [
                          ['class' => 'yii\grid\SerialColumn', 'header' => 'No','contentOptions' =>['style' => 'width:40px; text-align: center;']],

                          [
                              'class' => 'yii\grid\DataColumn',
                              'attribute' => 'Title/Description',
                              'contentOptions' =>['style' => 'width:250px'],
                              'content' => function ($model, $key, $index, $column)
                              {
                                  return $model->sourcingType->name;
                              },
                          ],
                          [
                              'class' => 'yii\grid\DataColumn',
                              'attribute' => 'Type',
                              'contentOptions' =>['style' => 'width:200px'],
                              'content' => function ($model, $key, $index, $column)
                              {
                                  return 'Admin support';
                              },
                          ],
                          [
                              'class' => 'yii\grid\DataColumn',
                              'attribute' => 'Requester',
                              'contentOptions' =>['style' => 'width:300px'],
                              'content' => function ($model, $key, $index, $column)
                              {
                                  return 'N/A';
                              },
                          ],
                          [
                              'class' => 'yii\grid\DataColumn',
                              'attribute' => 'Team',
                              'contentOptions' =>['style' => 'width:100px'],
                              'content' => function ($model, $key, $index, $column)
                              {
                                  $team = Yii::$app->params['team'];
                                  return $team[$model->team];
                              }
                          ],
                          [
                              'class' => 'yii\grid\DataColumn',
                              'attribute' => 'Total',
                              'contentOptions' =>['style' => 'width:100px'],
                              'content' => function ($model, $key, $index, $column)
                              {
                                  return $model->total;
                              }
                          ],
                      ],
                  ]); ?>
                <?php endif; ?>

                <?php if($expenseProvider): ?>
                  <?= GridView::widget([
                      'dataProvider' => $expenseProvider,
                      'summary' =>'',
                      'caption' =>'<strong>PART 4: EXPENSES SUMMARY</strong>',
                      'columns' => [
                          ['class' => 'yii\grid\SerialColumn', 'header' => 'No','contentOptions' =>['style' => 'width:40px; text-align: center;']],

                          [
                              'class' => 'yii\grid\DataColumn',
                              'attribute' => 'Title/Description',
                              'contentOptions' =>['style' => 'width:250px'],
                              'content' => function ($model, $key, $index, $column)
                              {
                                  return $model->sourcingType->name;
                              },
                          ],
                          [
                              'class' => 'yii\grid\DataColumn',
                              'attribute' => 'Type',
                              'contentOptions' =>['style' => 'width:200px'],
                              'content' => function ($model, $key, $index, $column)
                              {
                                  return 'Admin support';
                              },
                          ],
                          [
                              'class' => 'yii\grid\DataColumn',
                              'attribute' => 'Requester',
                              'contentOptions' =>['style' => 'width:300px'],
                              'content' => function ($model, $key, $index, $column)
                              {
                                  return $model->client->client_name;
                              },
                          ],
                          [
                              'class' => 'yii\grid\DataColumn',
                              'attribute' => 'Team',
                              'contentOptions' =>['style' => 'width:100px'],
                              'content' => function ($model, $key, $index, $column)
                              {
                                  $team = Yii::$app->params['team'];
                                  return $team[$model->team];
                              }
                          ],
                          [
                              'class' => 'yii\grid\DataColumn',
                              'attribute' => 'Total',
                              'contentOptions' =>['style' => 'width:100px'],
                              'content' => function ($model, $key, $index, $column)
                              {
                                  return $model->total;
                              }
                          ]
                      ],
                  ]); ?>
                <?php endif; ?>
              </div>

              <div class="text-right">
                <?= Html::a('Export to PDF', ['export', 'data' => Yii::$app->request->queryParams], ['class' => 'btn btn-info']) ?>
              </div>
        <?php endif; ?>
        <?php endif; ?>
    </div>
</div>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>

  <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        var tmp = new Array();
        tmp.push(Array('Researcher', 'Number of Resumes', 'Average Time(Minutes)'));
        <?php
          foreach($averageTime as $value){
        ?>
          tmp.push(Array(<?= "'" . $value['researcher'] . "'"?>, <?= $value['total']?>, <?= $value['time']?>));
        <?php } ?>
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable(tmp);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                           2]);

        var options = {
          title : 'Total Resumes by Researchers',
          width: <?= ($flag2 == 'active') ? '1200' : "'100%'" ?>,
          height: 500,
          chartArea: {width: '85%', height: '80%'},
          vAxis: {title: 'Number of Resumes'},
          hAxis: {title: 'Researcher'},
          seriesType: 'bars',
          legend: {
                position: 'none'
              },
          colors: ['#9843f2','#FF9900'],
          series: {1: {type: 'line'}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('columnChart'));
        chart.draw(view, options);
  }
  </script>

<script type="text/javascript">
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1.0', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);

    function drawChart() {
        var tmp = new Array();
        tmp.push(Array('Researcher', 'Error Resumes', 'Good Resumes'));
        <?php
          foreach($quanlity as $value){
        ?>
          tmp.push(Array(<?= "'" . $value['researcher'] . "'"?>, <?= $value['error']?>, <?= $value['total'] - $value['error']?>));
        <?php } ?>

        var data = google.visualization.arrayToDataTable(tmp);

        var options = {
          title: "Quanlity of Resumes by Researcher",
          width: <?= ($flag2 == 'active') ? '1200' : "'100%'" ?>,
          height: 600,
          chartArea: {width: '85%', height: '80%'},
          legend: { position: 'bottom', maxLines: 3 },
          colors: ['#ff9900','#109618'],
          vAxis: {
            minValue: 0,
            title: 'Resumes',
            ticks: [0, .2, .4, .6, .8, 1]
          },
          isStacked: 'percent',
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('stackChart'));

        chart.draw(data, options);
    }
</script>

<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        var tmp = new Array();
        tmp.push(Array('Recruiter', 'Number of Requests'));
        <?php
          foreach($RequestByRecruiters as $value){
        ?>
          tmp.push(Array(<?= '"' . $value['recruiter'] . '"'?>, <?= $value['total']?>));
        <?php } ?>

        var data = google.visualization.arrayToDataTable(tmp);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" }
                         ]);

        var options = {
          title: "Total Requests by Recruiters",
          width: <?= ($flag2 == 'active') ? '1200' : "'100%'" ?>,
          height: 500,
          colors: ['#1c91a5'],
          chartArea: {width: '75%', height: '70%'},
          legend: {
            position: 'none'
          },
          hAxis: {title: 'Number of Requests'}
        };
        var chart = new google.visualization.BarChart(document.getElementById("barChart"));
        chart.draw(view, options);
    }
  </script>

  

