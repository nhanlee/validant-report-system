<?php

namespace ext\cvparser;

/**
 * Common interface of Parser engine
 */
interface IParserService
{

    /**
     * @param string $content
     */
    public function parse($content);
    public function getResult();
}
