<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use app\models\SourcingType;

/**
 * This is the model class for table "expense_report".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $sourcer_id
 * @property integer $client_id
 * @property integer $team
 * @property integer $beginning
 * @property integer $done
 * @property integer $old
 * @property integer $created_by
 * @property string $added
 * @property string $updated
 * @property string $note
 */
class ExpenseReport extends \yii\db\ActiveRecord
{
    public $total = 0;
    
    /**
     * @Relations
     */
    public function getSourcer()
    {
        return $this->hasOne(Sourcer::className(), ['sourcer_id' => 'sourcer_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['client_id' => 'client_id']);
    }

    public function getSourcingType()
    {
        return $this->hasOne(SourcingType::className(), ['id' => 'type']);
    }

    /* Getter for client name */
    public function getClientName() {
        return $this->client->client_name;
    }

    /* Getter for team */
    public function getTeamName() {
        $team = Yii::$app->params['team'];
        return $team[$this->team];
    }

    /* Getter for team */
    public function getTypeName() {
        return $this->sourcingType->name;
    }

    /* Getter for team */
    public function getSourcerName() {
        return $this->sourcer->sourcer_name . ' ' . $this->sourcer->sourcer_surname;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'added',
                'updatedAtAttribute' => 'updated',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expense_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team', 'beginning', 'done', 'old', 'created_by'], 'integer'],
            [['type','client_id','sourcer_id', 'beginning', 'done'], 'required'],
            // [['beginning'], 'compare', 'compareAttribute'=>'done', 'operator'=>'>='],
            [['added', 'updated','note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'sourcer_id' => 'Sourcer',
            'client_id' => 'Requested By',
            'team' => 'Team',
            'beginning' => 'Beginning',
            'done' => 'Done',
            'old' => 'Old',
            'created_by' => 'Created By',
            'added' => 'Date',
            'updated' => 'Updated',
            'note' => 'Note',
            'clientName' => Yii::t('app', 'Requested By'),
            'teamName' => Yii::t('app', 'Team'),
            'typeName' => Yii::t('app', 'Type'),
            'sourcerName' => Yii::t('app', 'Sourcer'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(empty($this->old)){
            $this->old = 0;
        }
        if (parent::beforeSave($insert)) {
            if(!is_numeric($this->type)){
                $sourcingType = new sourcingType();
                $sourcingType->name = $this->type;
                $sourcingType->type = sourcingType::TYPE_EXPENSE;
                $sourcingType->save();
                $this->type = $sourcingType->getPrimaryKey();
            }
            return true;
        } else {
            return false;
        }
    }
}
