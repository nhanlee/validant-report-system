<?php

namespace ext\cvparser;

/**
 * Parser Resume
 *
 * @since Sprint25
 *
 * <code>
 * $rexParserService = new RexParserService();
 * $parser new CvParser($rexParserService);
 * $cvContent = file_get_contents($path);
 * if($parser->parse($cvContent)){
 *      $candidate = $parser->getCandidate();
 * }
 * </code>
 */
class CvParser
{

    /**
     * @var IParserService
     */
    protected $_service;

    /**
     * @var array
     */
    protected $_result;

    /**
     * @var Candidate
     */
    protected $_candidate;

    /**
     * @var CDetails candidate detail
     */
    protected $_candidateDetail;

    /**
     * @var CAddress candidate address
     */
    protected $_candidateAddress;

    /**
     * @var CEducation[] array of candidate education
     */
    protected $_candidateEducations;

    /**
     * @var CEmployment[] array of candidate working history
     */
    protected $_candidateExperiences;

    /**
     *
     * @var CHtml
     */
    protected $_candidateHtml;

    /**
     * Constructor, an IParserService is require
     *
     * @param IParserService $service
     */
    public function __construct(IParserService $service)
    {
        $this->setParserService($service);
    }

    /**
     * Get candidate
     * @return Candidate
     */
    public function getCandidate()
    {
        return $this->_candidate;
    }

    /**
     *
     * Get Cv HTML
     * return CHtml
     */
    public function getCandidateHtml(){
    	return $this->_candidateHtml;
    }

    /**
     * Get candidate detail
     *
     * @return CDetails
     */
    public function getCandidateDetail()
    {
        return $this->_candidateDetail;
    }

    /**
     * Get candidate address
     *
     * @return CAddress
     */
    public function getCandidateAddress()
    {
        return $this->_candidateAddress;
    }

    /**
     * Get education of candidate
     *
     * @return CEducation[] array of candidate working history
     */
    public function getCandidateEducation()
    {
        return $this->_candidateEducations;
    }

    /**
     * Get working history of candidate
     *
     * CEmployment[] array of candidate working history
     */
    public function getCandidateWorkingHistory()
    {
        return $this->_candidateExperiences;
    }

    /**
     * Get ParserService
     *
     * @return IParserService
     */
    public function getParserService()
    {
        return $this->_service;
    }

    /**
     * Set ParserService
     * @param IParserService $service
     */
    public function setParserService(IParserService $service)
    {
        $this->_service = $service;
    }

    /**
     * Parse Resume of candidate
     *
     * @param string $content binary data of Resume
     * @return boolean
     */
    public function parse($content)
    {
        try {
            if ($this->getParserService()->parse($content) === true) {
                $this->_result = $this->getParserService()->getResult();
                $this->_createCandidateInstance();
                return true;
            }
            return false;
        } catch (Exception $exc) {
            Yii::log($exc->getMessage(), CLogger::LEVEL_ERROR);
            return false;
        }
    }

    /**
     * Parse Resume of candidate
     *
     * @param string $content binary data of Resume
     * @return boolean
     */
    public function shortParse($content)
    {
        try {
            if ($this->getParserService()->shortParse($content) === true) {
                $this->_result = $this->getParserService()->getResult();
                $this->_createShortCandidateInstance();
                return true;
            }
            return false;
        } catch (Exception $exc) {
            Yii::log($exc->getMessage(), CLogger::LEVEL_ERROR);
            return false;
        }
    }

    /**
     * Create instances of candidate and its relation data
     * from Parser Service
     */
    protected function _createCandidateInstance()
    {
        $result = $this->_result;
        //1. Candidate
        $cand = new Candidate();
        $basic = $result['basic'];
        $cand->candidate_name = $basic['name'];
        $cand->candidate_surname = $basic['surname'];
        $cand->candidate_email = $basic['email'];
        $cand->candidate_country = $basic['country'];

        $this->_candidate = $cand;

        //2. Candidate detail
        $details = new CDetails();
        $d = $result['detail'];
        $details->c_details_summary = $d['summary'];
        $details->c_details_highestindustry = $d['highestindustry'];
        $details->c_details_management = $d['management'];
        $details->c_details_topskills = $d['topskills'];
        $details->c_details_toplevel = $d['toplevel'];
        $this->_candidateDetail = $details;
        $cand->detail = $details;
        $cand->addRelatedRecord('detail', $details, false);

        //3. Address
        $add = $result['address'];
        $address = new CAddress();
        $address->c_address_address1 = $add['address1'];
        $address->c_address_homephone = $add['homephone'];
        $address->c_address_mobile = $add['mobile'];
        $address->c_address_country = $add['country'];
        $address->c_address_region = $add['region'];
        $address->c_address_municipality = $add['municipality'];
        $address->c_address_postal = $add['postal'];
        $this->_candidateAddress = $address;

        $cand->addRelatedRecord('address', $address, false);

        //4. Education
        $edu = $result['edu'];
        $this->_candidateEducations = array();
        foreach ($edu as $v) {
            $e = new CEducation();
            $e->school_type = $v['school_type'];
            $e->school_name = $v['school_name'];
            $e->region = $v['region'];
            $e->country = $v['country'];
            $e->municipality = $v['municipality'];
            $e->degree_name = $v['degree_name'];
            $e->degree_major = $v['degree_major'];
            $e->start_year = $v['start_year'];
            $e->end_year = $v['end_year'];

            $this->_candidateEducations[] = $e;

            $cand->addRelatedRecord('educations', $e, true);
        }
        //5. Working history
        $this->_candidateExperiences = array();
        foreach ($result['work'] as $v) {
            $ex = new CEmployment();
            $ex->c_employment_employer_name = $v['employer'];
            $ex->c_employment_title = $v['title'];
            $ex->c_employment_job_description = $v['summarytext'];
            $ex->c_employment_job_start = $v['startdate'];
            $ex->c_employment_job_end = $v['enddate'];

            $this->_candidateExperiences[] = $ex;

            $cand->addRelatedRecord('experiences', $ex, true);
        }

        //6. Html Cv
        $cHtml = new CHtml();
        $cHtml->c_html = $result['html'];
        $this->_candidateHtml = $cHtml;

        $cand->addRelatedRecord('chtml', $cHtml, false);
    }

    /**
     * Create instances of candidate and its relation data
     * from Parser Service
     */
    protected function _createShortCandidateInstance()
    {
        $result = $this->_result;
        $basic = $result['basic'];
        $this->_candidate = $basic['name'] . ' ' . $basic['surname'];
    }
    /**
     * Verify duplication of email
     *
     * @return boolean TRUE if email is already existed in database
     */
    public function checkDuplicatedEmail()
    {
        if (!$this->getCandidate()->candidate_email) {
            return false;
        }
        $email = $this->getCandidate()->candidate_email;

        return Candidate::isDuplicated($email);
    }

    /**
     * Convert candidate info to JSON
     *
     * @return string JSON string of candidate data
     */
    public function toJson()
    {
        $data = $this->toArray();
        return CJSON::encode($data);
    }

    /**
     * Convert to association array
     *
     * @return array association array of candidate info
     * array('candidate'=>array(), 'detail'=>array(), 'address'=>array(), 'education'=>array(), 'workinghistory'=>array())
     */
    public function toArray()
    {
        $data = array();
        $data['candidate'] = $this->getCandidate()->attributes;
        $data['address'] = $this->getCandidateAddress()->attributes;
        $data['detail'] = $this->getCandidateDetail()->attributes;
        $data['chtml'] = $this->getCandidateHtml()->attributes;

        $data['education'] = array();
        foreach ($this->getCandidateEducation() as $v) {
            $data['education'][] = $v->attributes;
        }
        $data['workinghistory'] = array();
        foreach ($this->getCandidateWorkingHistory() as $v) {
            $data['workinghistory'][] = $v->attributes;
        }
        return $data;
    }

}
