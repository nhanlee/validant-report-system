<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sourcing-report-search clearfix">

    <?php $form = ActiveForm::begin([
        'method' => 'get',
        'action' => Yii::$app->homeUrl . '/site/report'
    ]); ?>
    <div class="row">
        <div class="col-md-4 clearfix">
            <?= $form->field($model, 'added')->widget(DateRangePicker::classname(),[
                'presetDropdown'=>true,
                'hideInput'=>false,
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'timePicker'=>false,
                    'separator'=>' -- ',
                    'format'=>'Y-m-d'
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group text-right col-md-4">
        <?= Html::submitButton('Filter', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
