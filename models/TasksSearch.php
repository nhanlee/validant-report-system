<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tasks;

/**
 * TasksSearch represents the model behind the search form about `app\models\Tasks`.
 */
class TasksSearch extends Tasks
{
    public $report = 0;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'emailID'], 'integer'],
            [['date', 'subject', 'recruiter', 'candidate', 'start', 'end', 'added', 'updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tasks::find();
        if(empty($params['sort'])){
            $query = Tasks::find()->orderBy('date DESC');
        }

        if($this->report){

        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'emailID' => $this->emailID,
            'start' => $this->start,
            'end' => $this->end,
            'added' => $this->added,
            'updated' => $this->updated,
        ]);

        if($this->date && $this->report){
            $tmp = explode(' - ', $this->date);
            $from = trim($tmp[0]) . ' 00:00:00';
            $to = trim($tmp[1]) . ' 59:59:59';
            $query->andFilterWhere(['>=', 'date', $from]);
            $query->andFilterWhere(['<=', 'date', $to]);
        }

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'recruiter', $this->recruiter])
            ->andFilterWhere(['like', 'candidate', $this->candidate]);

        return $dataProvider;
    }
}
