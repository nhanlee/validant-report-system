<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sourcer".
 *
 * @property integer $sourcer_id
 * @property string $sourcer_name
 * @property string $sourcer_surname
 * @property string $sourcer_email
 * @property integer $sourcer_active
 * @property integer $sourcer_added
 * @property integer $sourcer_updated
 */
class Sourcer extends \yii\db\ActiveRecord
{
    const ACTIVE         = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sourcer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sourcer_name', 'sourcer_surname', 'sourcer_email', 'sourcer_added', 'sourcer_updated'], 'required'],
            [['sourcer_active', 'sourcer_added', 'sourcer_updated'], 'integer'],
            [['sourcer_name', 'sourcer_surname', 'sourcer_email'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sourcer_id' => 'Sourcer ID',
            'sourcer_name' => 'Sourcer Name',
            'sourcer_surname' => 'Sourcer Surname',
            'sourcer_email' => 'Sourcer Email',
            'sourcer_active' => 'Sourcer Active',
            'sourcer_added' => 'Sourcer Added',
            'sourcer_updated' => 'Sourcer Updated',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getText()
    {
        return $this->sourcer_name . ' ' . $this->sourcer_surname;
    }
}
