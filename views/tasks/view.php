<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */

$this->title = $model->candidate;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tasks-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php 
        if(Yii::$app->user->identity->group == 1): ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= 
                Html::a("<span class='glyphicon glyphicon-upload'>Attachments</span>", 'javascript:void(0)', [
                               'title' => Yii::t('app', 'Upload'),
                               'class'=>'btn btn-info oldfile',
                           ]);
            ?>
            <?= 
                Html::a("<span class='glyphicon glyphicon-upload'>Formatted Resume</span>", 'javascript:void(0)', [
                               'title' => Yii::t('app', 'Upload Formatted Resume'),
                               'class'=>'btn btn-inverse newfile',
                           ]);
            ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [                      // the owner name of the model
                'label' => 'Date',
                'value' => date_format(new DateTime($model->date), 'd M, Y'),
            ],
            [                      // the owner name of the model
                'label' => 'Subject',
                'value' => Html::a($model->email->subject, ['view-content', 'email' => $model->email->id], ['target' => '_blank']),
                'format' => 'html'
            ],
            'recruiter',
            'address',
            'candidate',
            [                      // the owner name of the model
                'label' => 'Status',
                'value' => $model->getHtmlStatus(),
                'format' => 'html'
            ],
            [                      // the owner name of the model
                'label' => 'Attachments',
                'value' => $model->getHtmlCvs(),
                'format' => 'html'
            ],
            'note',
            [                      // the owner name of the model
                'label' => 'Resume Uploaded',
                'value' => $model->getHtmlNewCvs(),
                'format' => 'html'
            ],
        ],
    ]) ?>

</div>

<div class="modalUpload modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <?php
                    echo FileInput::widget([
                        'name' => 'cvs',
                        'id' => 'input-id',
                        'pluginOptions' => [
                            'uploadExtraData' => [
                                'task_id' => $model->id
                            ],
                            'showRemove' => false,
                            'maxFileCount' => 1
                            // 'allowedFileExtensions' => ['doc', 'docx', 'pdf', 'rtf'],
                        ]
                    ]);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<?php
$homeUrl = Yii::$app->homeUrl;
$this->registerJs("
    app = {
        homeUrl: '$homeUrl',
    };
", \yii\web\View::POS_HEAD);
?>
