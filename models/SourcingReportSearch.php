<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SourcingReport;

/**
 * SourcingReportSearch represents the model behind the search form about `app\models\SourcingReport`.
 */
class SourcingReportSearch extends SourcingReport
{
    public $report = 0;

    /* your calculated attribute */
    public $clientName;
    public $teamName;
    public $sourcerName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sourcer_id', 'client_id', 'team', 'created_by'], 'integer'],
            [['candidate', 'added', 'updated', 'clientName', 'teamName', 'sourcerName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SourcingReport::find()->orderBy('added DESC');
        if($this->report){
            $query = SourcingReport::find()->select('*, count(*) as total')
                                           ->groupBy('team, type')
                                           ->orderBy('added DESC');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if(!$this->report){
            $dataProvider->setSort([
            'attributes' => [
                    'added',
                    'clientName' => [
                        'asc' => ['clients.client_name' => SORT_ASC],
                        'desc' => ['clients.client_name' => SORT_DESC],
                        'label' => 'Recruiter'
                    ],
                    'teamName' => [
                        'asc' => ['team' => SORT_ASC],
                        'desc' => ['team' => SORT_DESC],
                        'label' => 'Team'
                    ],
                    'sourcerName' => [
                        'asc' => ['sourcer_name' => SORT_ASC, 'sourcer_surname' => SORT_ASC],
                        'desc' => ['sourcer_name' => SORT_DESC, 'sourcer_surname' => SORT_DESC],
                        'label' => 'Sourcer',
                        'default' => SORT_ASC
                    ],
                ]
            ]);

            $query->joinWith(['client']);
            $query->joinWith(['sourcer']);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sourcer_id' => $this->sourcer_id,
            'client_id' => $this->client_id,
            'team' => $this->team,
            'created_by' => $this->created_by,
            'updated' => $this->updated,
        ]);

        if($this->added && $this->report){
            $tmp = explode(' - ', $this->added);
            $from = trim($tmp[0]) . ' 00:00:00';
            $to = trim($tmp[1]) . ' 59:59:59';
            $query->andFilterWhere(['>=', 'added', $from]);
            $query->andFilterWhere(['<=', 'added', $to]);
        }
        $query->andFilterWhere(['like', 'candidate', $this->candidate]);

        if($this->sourcerName){
            $query->andWhere("CONCAT(sourcer.`sourcer_name`,' ',sourcer.`sourcer_surname`) LIKE '%" . $this->sourcerName . "%'");
        }

        return $dataProvider;
    }
}
