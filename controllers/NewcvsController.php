<?php

namespace app\controllers;

use Yii;
use app\models\Newcvs;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Tasks;

/**
 * CvsController implements the CRUD actions for Cvs model.
 */
class NewcvsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cvs models.
     * @return mixed
     */
    public function actionFileUpload()
    {
        $post = Yii::$app->request->post();
        if (empty($_FILES['cvs']) || empty($post['task_id']) ) {
          echo json_encode(['error'=>'No files found for upload.']);
          return; // terminate
        }

        // get the files posted
        $file = $_FILES['cvs'];

        // a flag to see if everything is ok
        $success = false;

        // file paths to store
        $output = [];

        // get file names
        $filenames = $file['name'];

        $target = Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . "newcvs" . DIRECTORY_SEPARATOR . $filenames;
        if(move_uploaded_file($file['tmp_name'], $target)) {
            $success = true;
        } else {
            $success = false;
        }

        if ($success === true) {
          $newcvs = Newcvs::find()->where(['taskID' => $post['task_id']])->one();
          if($newcvs){
              //remove old file and update new file
              $old = Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . "newcvs" . DIRECTORY_SEPARATOR . $newcvs->name;
              if(file_exists($old)){
                  unlink($old);
              }
              $newcvs->name = $filenames;
          } else {
              // Add cvs record
              $newcvs = new Newcvs();
              $newcvs->name = $filenames;
              $newcvs->taskID = $post['task_id'];
          }
          if($newcvs->save()){
              // Set status for task
              $task = Tasks::findOne($post['task_id']);
              $task->status = Tasks::STATUS_UPLOADED;
              $task->save();
              $output = [
                'uploaded' => $success,
                'uploadedHtml' => $task->getHtmlStatus()
                ];
          }
        } elseif ($success === false) {
          $output = ['error'=>'Error while uploading file. Contact the system administrator'];
          unlink($target);
        } else {
          $output = ['error'=>'No files were processed.'];
        }

        // return a json encoded response for plugin to process successfully
        echo json_encode($output);

    }

    public function actionAdminFileUpload()
    {
        $post = Yii::$app->request->post();
        if (empty($_FILES['cvs']) || empty($post['task_id']) ) {
          echo json_encode(['error'=>'No files found for upload.']);
          return; // terminate
        }

        // get the files posted
        $file = $_FILES['cvs'];

        // a flag to see if everything is ok
        $success = false;

        // file paths to store
        $output = [];

        // get file names
        $filenames = $file['name'];

        $newcvs = Newcvs::find()->where(['taskID' => $post['task_id']])->one();
        if($newcvs){
            //remove old file and update new file
            $old = Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . "newcvs" . DIRECTORY_SEPARATOR . $newcvs->name;
            if(file_exists($old)){
                unlink($old);
            }
        } else {
            // Add cvs record
            $newcvs = new Newcvs();
        }
        $newcvs->name = $filenames;
        $newcvs->taskID = $post['task_id'];
        if($newcvs->save()){
            //Move from tmp to upload newcvs
            $target = Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . "newcvs" . DIRECTORY_SEPARATOR . $filenames;
            if(move_uploaded_file($file['tmp_name'], $target)) {
                $success = true;
            } else {
                $success = false;
            }
            $output = ['success'=>'Uploaded.'];
        }

        // return a json encoded response for plugin to process successfully
        echo json_encode($output);

    }

    protected function findModel($id)
    {
        if (($model = Newcvs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
