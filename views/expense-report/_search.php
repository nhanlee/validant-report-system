<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\SourcingType;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ExpenseReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="expense-report-search clearfix">

    <?php $form = ActiveForm::begin([
        'action' => ['report'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList(
                                                    ArrayHelper::map(SourcingType::find()->where(['type' => SourcingType::TYPE_EXPENSE])
                                                                                        ->orderBy('name')
                                                                                        ->all(), 'id', 'name'),
                                                    ['prompt' => '--Choose type--']
                                                ) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 clearfix">
            <?= $form->field($model, 'added')->widget(DateRangePicker::classname(),[
                'presetDropdown'=>true,
                'hideInput'=>false,
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'timePicker'=>false,
                    'separator'=>' -- ',
                    'format'=>'Y-m-d'
                ]
            ]) ?>
        </div>
    </div>

    <div class="col-md-4 form-group text-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
