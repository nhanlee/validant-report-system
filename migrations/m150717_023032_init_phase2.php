<?php

use yii\db\Schema;
use yii\db\Migration;

class m150717_023032_init_phase2 extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE `cvs` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) DEFAULT NULL,
                  `emailID` int(11) DEFAULT NULL,
                  `taskID` int(11) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
                ";
        $this->execute($sql);

        $sql = "CREATE TABLE `email` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  `subject` varchar(255) DEFAULT NULL,
                  `recruiter` varchar(255) DEFAULT NULL,
                  `address` varchar(45) DEFAULT NULL,
                  `content` text,
                  `text` text,
                  `status` int(3) DEFAULT '0',
                  `number` int(11) DEFAULT NULL,
                  `added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

                ";
        $this->execute($sql);

        $sql = "CREATE TABLE `newcvs` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) DEFAULT NULL,
                  `taskID` int(11) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
        $this->execute($sql);

        $sql = "CREATE TABLE `tasks` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  `subject` varchar(255) DEFAULT NULL,
                  `recruiter` varchar(255) DEFAULT NULL,
                  `candidate` varchar(255) DEFAULT NULL,
                  `status` int(3) DEFAULT '0',
                  `emailID` int(11) DEFAULT NULL,
                  `start` timestamp NULL DEFAULT NULL,
                  `end` timestamp NULL DEFAULT NULL,
                  `reviewed` varchar(255) DEFAULT NULL,
                  `note` varchar(255) DEFAULT NULL,
                  `user` int(11) DEFAULT NULL,
                  `added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150717_023032_init_phase2 cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
