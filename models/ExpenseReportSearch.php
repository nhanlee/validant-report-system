<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ExpenseReport;

/**
 * ExpenseReportSearch represents the model behind the search form about `app\models\ExpenseReport`.
 */
class ExpenseReportSearch extends ExpenseReport
{
    public $report = 0;

    /* your calculated attribute */
    public $clientName;
    public $teamName;
    public $typeName;
    public $sourcerName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'team', 'beginning', 'done', 'created_by'], 'integer'],
            [['client_id', 'added', 'updated', 'clientName', 'teamName', 'typeName','sourcerName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ExpenseReport::find()->orderBy('added DESC');
        if($this->report){
            $query = ExpenseReport::find()->select('*, sum(`done`) + sum(`old`) as total')
                                           ->groupBy('type')
                                           ->orderBy('added DESC');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if(!$this->report){
            $dataProvider->setSort([
            'attributes' => [
                    'added',
                    'beginning',
                    'done',
                    'clientName' => [
                        'asc' => ['clients.client_name' => SORT_ASC],
                        'desc' => ['clients.client_name' => SORT_DESC],
                        'label' => 'Recruiter'
                    ],
                    'teamName' => [
                        'asc' => ['team' => SORT_ASC],
                        'desc' => ['team' => SORT_DESC],
                        'label' => 'Team'
                    ],
                    'typeName' => [
                        'asc' => ['sourcing_type.name' => SORT_ASC],
                        'desc' => ['sourcing_type.name' => SORT_DESC],
                        'label' => 'Type'
                    ],
                    'sourcerName' => [
                        'asc' => ['sourcer_name' => SORT_ASC, 'sourcer_surname' => SORT_ASC],
                        'desc' => ['sourcer_name' => SORT_DESC, 'sourcer_surname' => SORT_DESC],
                        'label' => 'Sourcer',
                        'default' => SORT_ASC
                    ],
                ]
            ]);

            $query->joinWith(['client']);
            $query->joinWith(['sourcingType']);
            $query->joinWith(['sourcer']);
        }
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'client_id' => $this->client_id,
            'team' => $this->team,
            'beginning' => $this->beginning,
            'done' => $this->done,
            'created_by' => $this->created_by,
            'updated' => $this->updated,
        ]);

        if($this->added && $this->report){
            $tmp = explode(' - ', $this->added);
            $from = trim($tmp[0]) . ' 00:00:00';
            $to = trim($tmp[1]) . ' 59:59:59';
            $query->andFilterWhere(['>=', 'added', $from]);
            $query->andFilterWhere(['<=', 'added', $to]);
        }

        if($this->sourcerName){
            $query->andWhere("CONCAT(sourcer.`sourcer_name`,' ',sourcer.`sourcer_surname`) LIKE '%" . $this->sourcerName . "%'");
        }

        return $dataProvider;
    }
}
