<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ExpenseReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Expense Reports';
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('inputFinish')): ?>
        <div class="alert alert-success">
            <?php echo Yii::$app->session->getFlash('inputFinish'); ?>
        </div>
    <?php endif; ?>

    <?php  echo $this->render('create', ['model' => $model]); ?>
    <hr>
<div class="expense-report-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'attribute' => 'added',
                'filter' => false,
                'format' => ['date', 'php:d M, Y']
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'typeName',
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'sourcerName',
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'clientName',
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'teamName',
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'beginning',
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'done',
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Remain',
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->beginning - $model->done;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'old',
                'filter' => false,
            ],
            'note',
            // 'created_by',
            // 'added',
            // 'updated',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}']
        ],
    ]); ?>

</div>
