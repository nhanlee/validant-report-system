<?php

namespace app\models\entitydb;

use Yii;

class legacyDB
{
		protected $db;
		public $from;
		public	$to;

		public function __construct()
    {
        $this->db = \Yii::$app->legacyDB;
    }

		//New jobs open in the month
		public function getNewJobs()
    {
				$sql ="SELECT count(*) as newjobs FROM jobs WHERE job_company= 16 AND job_added >=:from AND job_added <:to";
				$newjobs = $this->db->createCommand($sql)
												 ->bindValue(':from', $this->from)
												 ->bindValue(':to', $this->to)
            						 ->queryScalar();
				return $newjobs;
    }

		//Total jobs run in the month
		public function getJobsHasSubmission()
    {
				$sql ="SELECT count(*) as jobssubmission
							FROM submitted s
							JOIN jobs j ON s.submitted_job = j.job_id
							WHERE j.job_company =16 AND s.submitted_status IN(0,1,2,7,8) AND s.submitted_date >=:from AND s.submitted_date <:to
							GROUP BY j.job_id";
				$jobssubmission = $this->db->createCommand($sql)
												 ->bindValue(':from', $this->from)
												 ->bindValue(':to', $this->to)
            						 ->queryScalar();
				if($jobssubmission){
					return $jobssubmission;
				}
				return 0;
    }

		//Total jobs run in the month
		public function getJobsHasSubmissionEU()
    {
				$sql ="SELECT count(*) as total FROM submitted s
							JOIN jobs j ON s.submitted_job = j.job_id
							WHERE j.job_refno='EU' AND j.job_company =16 AND s.submitted_status IN(0,1,2,7,8) AND s.submitted_date >=:from AND s.submitted_date <:to";
				$resumeSentEU = $this->db->createCommand($sql)
												 ->bindValue(':from', $this->from)
												 ->bindValue(':to', $this->to)
            						 ->queryScalar();
				return $resumeSentEU;
    }

		//Total jobs run in the month
		public function getJobsHasSubmissionUS()
    {
				$sql ="SELECT count(*) as total FROM submitted s
							JOIN jobs j ON s.submitted_job = j.job_id
							WHERE j.job_refno<>'EU' AND j.job_company =16 AND s.submitted_status IN(0,1,2,7,8) AND s.submitted_date >=:from AND s.submitted_date <:to";
				$resumeSentUS = $this->db->createCommand($sql)
												 ->bindValue(':from', $this->from)
												 ->bindValue(':to', $this->to)
            						 ->queryScalar();
				return $resumeSentUS;
    }

		//Total jobs run in the month
		public function getJobsHasSubmissionEUNew()
    {
				$sql ="SELECT count(*) as total FROM submitted s
							JOIN bh_cache bh ON bh.submitted = s.submitted_id
							JOIN jobs j ON s.submitted_job = j.job_id
							WHERE bh.status=1 AND j.job_refno='EU' AND j.job_company =16 AND s.submitted_status IN(0,1,2,7,8) AND s.submitted_date >=:from AND s.submitted_date <:to";
				$resumeSentUS = $this->db->createCommand($sql)
												 ->bindValue(':from', $this->from)
												 ->bindValue(':to', $this->to)
            						 ->queryScalar();
				return $resumeSentUS;
    }

		//Total jobs run in the month
		public function getJobsHasSubmissionUSNew()
    {
				$sql ="SELECT count(*) as total FROM submitted s
							JOIN bh_cache bh ON bh.submitted = s.submitted_id
							JOIN jobs j ON s.submitted_job = j.job_id
							WHERE bh.status=1 AND j.job_refno<>'EU' AND j.job_company =16 AND s.submitted_status IN(0,1,2,7,8) AND s.submitted_date >=:from AND s.submitted_date <:to";
				$resumeSentUS = $this->db->createCommand($sql)
												 ->bindValue(':from', $this->from)
												 ->bindValue(':to', $this->to)
            						 ->queryScalar();
				return $resumeSentUS;
    }

		//Total jobs run in the month
		public function getResumesAccepted()
    {
				$sql ="SELECT count(*) as total FROM submitted s
							JOIN jobs j ON s.submitted_job = j.job_id
							WHERE j.job_company =16 AND s.submitted_status=1 AND s.submitted_date >=:from AND s.submitted_date <:to";
				$total = $this->db->createCommand($sql)
												 ->bindValue(':from', $this->from)
												 ->bindValue(':to', $this->to)
            						 ->queryScalar();
				return $total;
    }

		//Get part2
		public function getPart2()
    {
			$data = array();
			$sql ="SELECT j.job_refno, j.job_title,
			 			COUNT(*) as `total`,
						SUM(IF(bh.status = 1, 1, 0)) as `new`
						FROM submitted s
						JOIN jobs j ON s.submitted_job = j.job_id
						LEFT JOIN bh_cache bh ON bh.submitted = s.submitted_id
						WHERE j.job_company =16 AND s.submitted_status IN(0,1,2,7,8) AND s.submitted_date >=:from AND s.submitted_date <:to
						GROUP BY j.job_id
						ORDER BY j.job_refno, j.job_title";
			$results = $this->db->createCommand($sql)
											->bindValue(':from', $this->from)
											->bindValue(':to', $this->to)
											->queryAll();
			if($results){
		    	$team = ['EU','Red Team', 'White Team','Yellow Team','Eu'];
				foreach ($results as $r) {
					if (is_numeric($r['job_refno'])) {
						//Apply new way
						$title = explode("-", $r['job_title']);
						$title = ucwords(trim($title[0]));
						if(in_array($title, $team)){
							$data[$title][] = $r;
						} else {
							$data['No Team'][] = $r;
						}
					} else {
						//Refno is a team
						$data[$r['job_refno']][] = $r;
					}
				}
			}
			return $data;
    }
}
