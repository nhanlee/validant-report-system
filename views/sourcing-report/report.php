<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Sourcer;
use app\models\Clients;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SourcingReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sourcing Reports';
$this->params['breadcrumbs'][] = $this->title;
?>


    <h1><?= Html::encode($this->title) ?></h1>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="sourcing-report-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],

            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Type',
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->sourcingType->name;
                },
            ],
            // [
            //     'class' => 'yii\grid\DataColumn',
            //     'attribute' => 'Recruiter',
            //     'filter' => true,
            //     // 'filter' => ArrayHelper::map(Clients::find()->all(), 'client_id', 'client_name'),
            //     'content' => function ($model, $key, $index, $column)
            //     {
            //         return $model->client->client_name;
            //     }
            // ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Team',
                'content' => function ($model, $key, $index, $column)
                {
                    $team = Yii::$app->params['team'];
                    return $team[$model->team];
                }
            ],
            // [
            //     'class' => 'yii\grid\DataColumn',
            //     'attribute' => 'Date',
            //     'content' => function ($model, $key, $index, $column)
            //     {
            //         $date = new DateTime($model->added);
            //         return $date->format('d M y');
            //     },
            // ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Total',
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->total;
                }
            ],
        ],
    ]); ?>

</div>
