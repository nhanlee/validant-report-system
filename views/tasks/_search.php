<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\TasksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-search clearfix">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-4 clearfix">
            <?= $form->field($model, 'date')->widget(DateRangePicker::classname(),[
                'presetDropdown'=>true,
                'hideInput'=>false,
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'timePicker'=>false,
                    'separator'=>' -- ',
                    'format'=>'Y-m-d'
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group text-right col-md-4">    
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
