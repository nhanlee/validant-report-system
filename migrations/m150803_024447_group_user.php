<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_024447_group_user extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `user` ADD COLUMN `group` VARCHAR(45) NULL DEFAULT '0' AFTER `sourcer_id`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m150803_024447_group_user cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
