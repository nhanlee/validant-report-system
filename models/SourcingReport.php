<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use app\models\SourcingType;
/**
 * This is the model class for table "sourcing_report".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $sourcer_id
 * @property integer $client_id
 * @property integer $team
 * @property string $candidate
 * @property integer $created_by
 * @property string $added
 * @property string $updated
 * @property string $note
 */
class SourcingReport extends \yii\db\ActiveRecord
{
    public $total = 0;
    
    /**
     * @Relations
     */
    public function getSourcer()
    {
        return $this->hasOne(Sourcer::className(), ['sourcer_id' => 'sourcer_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['client_id' => 'client_id']);
    }

    public function getSourcingType()
    {
        return $this->hasOne(SourcingType::className(), ['id' => 'type']);
    }

    /* Getter for client name */
    public function getClientName() {
        return $this->client->client_name;
    }

    /* Getter for team */
    public function getTeamName() {
        $team = Yii::$app->params['team'];
        return $team[$this->team];
    }

    /* Getter for team */
    public function getSourcerName() {
        if ($this->sourcer) {
            return $this->sourcer->sourcer_name . ' ' . $this->sourcer->sourcer_surname;
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'added',
                'updatedAtAttribute' => 'updated',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sourcing_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sourcer_id', 'client_id', 'team', 'created_by'], 'integer'],
            [['type','sourcer_id', 'client_id', 'candidate'], 'required'],
            [['added', 'updated','note'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'sourcer_id' => 'Sourcer',
            'client_id' => 'Recruiter',
            'team' => 'Team',
            'candidate' => 'Candidate',
            'created_by' => 'Created By',
            'added' => 'Date',
            'updated' => 'Updated',
            'note' => 'Note',
            'clientName' => Yii::t('app', 'Recruiter'),
            'teamName' => Yii::t('app', 'Team'),
            'sourcerName' => Yii::t('app', 'Sourcer'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(!is_numeric($this->type)){
                $sourcingType = new sourcingType();
                $sourcingType->name = $this->type;
                $sourcingType->type = sourcingType::TYPE_SOURCING;
                $sourcingType->save();
                $this->type = $sourcingType->getPrimaryKey();
            }
            return true;
        } else {
            return false;
        }
    }

    public static function getValidantReport($from, $to){
			$sql ="SELECT COUNT(*) as total, `sourcing_type`.name as des
						FROM `sourcing_report` s
						LEFT JOIN `sourcing_type` ON `sourcing_type`.id = s.type
						WHERE s.`added` >=:from AND s.`added` <:to
						GROUP BY s.`type`
						ORDER BY total DESC";
			$results = \Yii::$app->db->createCommand($sql)
											->bindValue(':from', $from)
											->bindValue(':to', $to)
											->queryAll();
      return $results;
    }

}
