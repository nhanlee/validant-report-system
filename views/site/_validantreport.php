<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Sourcer;
use app\models\Clients;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SourcingReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Generate Validant Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
  $added = Yii::$app->request->queryParams['data']['ValidantReportSearch']['added'];
?>

    <h3 style="text-align: center">VALIDANT – WORK REPORT</h3>
    <h4 style="text-align: center"><?= $added ?></h4>

<div class="validant-report">
  <?php if($data['part1']): ?>
    <table class="table table-striped table-bordered">
      <caption><strong>PART 1: SUMMARY</strong></caption>
      <thead>
        <tr>
          <th style="width: 45px;text-align: center;">No</th>
          <th>Description</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($data['part1'] as $index => $d):?>
          <tr>
            <td style="text-align: center;"><?= $index+1; ?></td>
            <td><?= $d['des']; ?></td>
            <td><?= $d['total']; ?></td>
          </tr>
        <?php endforeach;?>
      </tbody>
    </table>
  <?php endif; ?>

  <?php if($data['part2']): ?>
    <table class="table table-striped table-bordered">
      <caption><strong>PART 2: REPORT ELLABORATION</strong></caption>
      <thead>
        <tr>
          <th style="width: 45px; text-align: center;">No</th>
          <th style="text-align:center;">Team</th>
          <th>Job</th>
          <th style="width: 115px">New Resume</th>
          <th style="width: 115px">Total</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $grandTotalNew = 0;
          $grandTotalTotal = 0;
        ?>
        <?php foreach($data['part2'] as $key => $team):?>
          <?php
            $totalNew = 0;
            $totalTotal = 0;
          ?>
          <?php foreach($team as $index => $job):?>
              <tr>
                <td style="text-align: center;"><?= $index+1; ?></td>
                  <?php if($index == 0):?>
                    <td style="vertical-align: middle; text-align:center; background-color: #ffffff;" rowspan="<?= count($team); ?>"><?= $key; ?></td>
                  <?php endif;?>
                <td><?= $job['job_title']; ?></td>
                <td><?= $job['new']; ?></td>
                <td><?= $job['total']; ?></td>
              </tr>
              <?php
                $totalNew += $job['new'];
                $totalTotal += $job['total'];
              ?>
          <?php endforeach;?>
              <tr style="  background-color: #a7be9c;">
                <td colspan="3" style="text-align: center;">TOTAL</td>
                <td><?= $totalNew; ?></td>
                <td><?= $totalTotal; ?></td>
              </tr>
          <?php
            $grandTotalNew += $totalNew;
            $grandTotalTotal += $totalTotal;
          ?>
        <?php endforeach;?>
              <tr style="  background-color: #a779a2;">
                <td colspan="3" style="text-align: center;">GRAND TOTAL</td>
                <td><?= $grandTotalNew; ?></td>
                <td><?= $grandTotalTotal; ?></td>
              </tr>
      </tbody>
    </table>
  <?php endif; ?>

  <?php if($sourcingProvider): ?>
    <?= GridView::widget([
        'dataProvider' => $sourcingProvider,
        'summary' =>'',
        'caption' =>'<strong>PART 3: ADMIN TASKS</strong>',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => 'No','contentOptions' =>['style' => 'width:40px; text-align: center;']],

            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Title/Description',
                'contentOptions' =>['style' => 'width:250px'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->sourcingType->name;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Type',
                'contentOptions' =>['style' => 'width:200px'],
                'content' => function ($model, $key, $index, $column)
                {
                    return 'Admin support';
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Requester',
                'contentOptions' =>['style' => 'width:300px'],
                'content' => function ($model, $key, $index, $column)
                {
                    return 'N/A';
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Team',
                'contentOptions' =>['style' => 'width:100px'],
                'content' => function ($model, $key, $index, $column)
                {
                    $team = Yii::$app->params['team'];
                    return $team[$model->team];
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Total',
                'contentOptions' =>['style' => 'width:100px'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->total;
                }
            ],
        ],
    ]); ?>
  <?php endif; ?>

  <?php if($expenseProvider): ?>
    <?= GridView::widget([
        'dataProvider' => $expenseProvider,
        'summary' =>'',
        'caption' =>'<strong>PART 4: EXPENSES SUMMARY</strong>',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => 'No','contentOptions' =>['style' => 'width:40px; text-align: center;']],

            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Title/Description',
                'contentOptions' =>['style' => 'width:250px'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->sourcingType->name;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Type',
                'contentOptions' =>['style' => 'width:200px'],
                'content' => function ($model, $key, $index, $column)
                {
                    return 'Admin support';
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Requester',
                'contentOptions' =>['style' => 'width:300px'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->client->client_name;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Team',
                'contentOptions' =>['style' => 'width:100px'],
                'content' => function ($model, $key, $index, $column)
                {
                    $team = Yii::$app->params['team'];
                    return $team[$model->team];
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Total',
                'contentOptions' =>['style' => 'width:100px'],
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->total;
                }
            ]
        ],
    ]); ?>
  <?php endif; ?>
</div>
