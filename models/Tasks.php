<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tasks".
 *
 * @property integer $id
 * @property string $date
 * @property string $subject
 * @property string $recruiter
 * @property string $candidate
 * @property integer $status
 * @property integer $emailID
 * @property string $start
 * @property string $end
 * @property string $added
 * @property string $note
 * @property string $address
 * @property string $updated
 */
class Tasks extends \yii\db\ActiveRecord
{
    const STATUS_AWAITING = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_UPLOADED = 2;
    const STATUS_DONE = 3;
    const STATUS_SENT = 4;
    /**
     * @Relations
     */
    public function getEmail()
    {
        return $this->hasOne(Email::className(), ['id' => 'emailID']);
    }

    public function getLoginUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }

    public function getCvs()
    {
        return $this->hasMany(Cvs::className(), ['taskID' => 'id']);
    }

    public function getNewcvs()
    {
        return $this->hasOne(Newcvs::className(), ['taskID' => 'id']);
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'start', 'end', 'added', 'updated','note','address'], 'safe'],
            [['status', 'emailID'], 'integer'],
            [['subject', 'recruiter', 'candidate'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'subject' => 'Subject',
            'recruiter' => 'Recruiter',
            'address' => 'Address',
            'candidate' => 'Candidate',
            'status' => 'Status',
            'emailID' => 'Email ID',
            'start' => 'Start',
            'end' => 'End',
            'reviewed' => 'Reviewed',
            'note' => 'Note',
            'added' => 'Added',
            'updated' => 'Updated',
        ];
    }

    public function getHtmlStatus(){
      switch ($this->status) {
        case self::STATUS_AWAITING:
            return '<span class="label label-default">Awaiting</span>';
          break;

        case self::STATUS_PROCESSING:
            return '<span class="label label-info">Processing</span>';
          break;

        case self::STATUS_UPLOADED:
            return '<span class="label label-warning">Uploaded</span>';
          break;

        case self::STATUS_DONE:
            return '<span class="label label-success">Done</span>';
          break;

        case self::STATUS_SENT:
            return '<span class="label label-primary">Sent</span>';
          break;

        default:
            return '<span class="label label-default">Awaiting</span>';
          break;
      }
    }

    public function getHtmlCvs()
    {
        $task_id = $this->isProcessing();
        $html = '';
        if($this->cvs){
                  $path = Yii::getAlias('@web') . '/cvs/';
            foreach ($this->cvs as $cv) {
                $name = $path . $cv->email->getPrimaryKey() . '_v1_' .  $cv->name;
                if(($task_id == $cv->taskID && $task_id > 0) || Yii::$app->user->identity->group == 1){
                    $html .= '<div><a href="' . $name . '">' . $cv->name . '</a></div>';
                } else {
                    $html .= "<div>$cv->name</div>";
                }
            }
        }
        return $html;
    }

    public function getHtmlNewCvs()
    {
        $html = '';
        if($this->newcvs){
            $name = $this->newcvs->name;
            $path = Yii::getAlias('@web') . '/newcvs/';
            $fullPathName = $path .  $name;
            $html .= '<div><a href="' . $fullPathName . '">' . $name . '</a></div>';
        }
        return $html;
    }

    public static function isProcessing(){
        $userID = Yii::$app->user->getId();
        $task = self::find()
                ->where('user= :userID AND (status=:status1 OR status=:status2)', [':userID' => $userID, ':status1' => self::STATUS_PROCESSING, ':status2' => self::STATUS_UPLOADED])
                ->one();
        // $task = self::find()->orWhere(['status' => self::STATUS_PROCESSING, 'status' => self::STATUS_UPLOADED])->where(['user' => $userID])->one();
        if($task){
            return $task->id;
        }
        return 0;
    }

    public static function averageTime($dateRange = null){
        $from = null;
        $to = null;
        $addition = 'AND 1';
        if(!empty($dateRange)){
            $tmp = explode(' - ', $dateRange);
            $from = trim($tmp[0]) . ' 00:00:00';
            $to = trim($tmp[1]) . ' 59:59:59';
        }
        if($from && $to){
            $addition = "AND t.added >= '$from' AND t.added <= '$to'";
        }
        $sql = "SELECT t.id, COUNT(*) as total, ROUND(AVG((UNIX_TIMESTAMP(end) - UNIX_TIMESTAMP(start))/60)) as time, t.user, CONCAT(s.sourcer_name,' ',s.sourcer_surname) as researcher
                FROM tasks t
                JOIN user u ON u.id = t.user
                JOIN sourcer s ON s.sourcer_id = u.sourcer_id
                WHERE t.status IN (3,4)
                $addition
                GROUP BY t.user
                ORDER BY time";
        $results = \Yii::$app->db->createCommand($sql)->queryAll();
        return $results;
    }

    public static function RequestByRecruiters($dateRange = null){
        $from = null;
        $to = null;
        $addition = '1';
        if(!empty($dateRange)){
            $tmp = explode(' - ', $dateRange);
            $from = trim($tmp[0]) . ' 00:00:00';
            $to = trim($tmp[1]) . ' 59:59:59';
        }
        if($from && $to){
            $addition = "t.added >= '$from' AND t.added <= '$to'";
        }
        $sql = "SELECT t.recruiter,COUNT(*) as total
                FROM tasks t
                WHERE $addition
                GROUP BY t.recruiter
                ORDER BY total";
        $results = \Yii::$app->db->createCommand($sql)->queryAll();
        return $results;
    }
    public static function Quanlity($dateRange = null){
        $from = null;
        $to = null;
        $addition = 'AND 1';
        if(!empty($dateRange)){
            $tmp = explode(' - ', $dateRange);
            $from = trim($tmp[0]) . ' 00:00:00';
            $to = trim($tmp[1]) . ' 59:59:59';
        }
        if($from && $to){
            $addition = "AND t.added >= '$from' AND t.added <= '$to'";
        }
        $sql = "SELECT t.id, COUNT(*) as total, CONCAT(s.sourcer_name,' ',s.sourcer_surname) as researcher, SUM(IF((t.note LIKE '%Error%' OR t.note LIKE '%sai%' OR t.note LIKE '%resend%'),1,0)) as error
                FROM tasks t
                JOIN user u ON u.id = t.user
                JOIN sourcer s ON s.sourcer_id = u.sourcer_id
                WHERE t.status IN (3,4)
                $addition
                GROUP BY t.user
                ORDER BY total DESC";
        $results = \Yii::$app->db->createCommand($sql)->queryAll();
        return $results;
    }
}
