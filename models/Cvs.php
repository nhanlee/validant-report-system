<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cvs".
 *
 * @property integer $id
 * @property string $name
 * @property integer $emailID
 * @property integer $taskID
 */
class Cvs extends \yii\db\ActiveRecord
{

    public function getEmail()
    {
        return $this->hasOne(Email::className(), ['id' => 'emailID']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cvs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emailID', 'taskID'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'emailID' => 'Email ID',
            'taskID' => 'Task ID',
        ];
    }
}
