<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ExpenseReport */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Expense Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $team = Yii::$app->params['team']; ?>

<div class="expense-report-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if(Yii::$app->user->identity->group == 1): ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [                      // the owner name of the model
                'label' => 'Type',
                'value' => $model->sourcingType->name,
            ],
            [                      // the owner name of the model
                'label' => 'Sourcer',
                'value' => $model->sourcer->getText(),
            ],
            [                      // the owner name of the model
                'label' => 'Requested By',
                'value' => $model->client->client_name,
            ],
            [                      // the owner name of the model
                'label' => 'Team',
                'value' => $team[$model->team],
            ],
            'beginning',
            'done',
            'old',
            'note',
            [                      // the owner name of the model
                'label' => 'Remain',
                'value' => $model->beginning - $model->done,
            ],
            'added',
            'updated',
        ],
    ]) ?>

</div>
