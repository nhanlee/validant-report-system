<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii;
use yii\console\Controller;
use Ddeboer\Imap\Server;
use Ddeboer\Imap\SearchExpression;
use Ddeboer\Imap\Search\Flag\Unseen;
use app\models\Email;
use app\models\Cvs;
use ext\cvparser\RexParserService;
use ext\cvparser\CvParser;
use app\models\Tasks;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CrawEmailController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($limit = 10)
    {
      $config = Yii::$app->params['imap'];
      $hostname = $config['host'];
      $username = $config['account'];
      $password = $config['password'];

      $server = new Server(
          $hostname,
          993,
          '/imap/ssl/novalidate-cert/norsh}Inbox'
      );
      $connection = $server->authenticate($username, $password);
      $mailbox = $connection->getMailbox('INBOX');
      $search = new SearchExpression();
      $search->addCondition(new Unseen());
      $messages = $mailbox->getMessages($search);
      $count = 1;
      foreach ($messages as $message) {
          $number = $message->getNumber();
          // $mail = Email::findOne(['number' => $number]);
          // if(!$mail){
              $date = $message->getDate();
              $date = $date->format('Y-m-d H:i:s');
              $subject = $message->getSubject();
              $recruiter = $message->getFrom()->getName();
              $address = $message->getFrom()->getAddress();
              $content = base64_encode(bzcompress($message->getBodyHtml()));
              $text = base64_encode(bzcompress($message->getBodyText()));

              //Create new emails
              $email = new Email();
              $email->date = $date;
              $email->subject = $subject;
              $email->recruiter = $recruiter;
              $email->address = $address;
              $email->content = $content;
              $email->text = $text;
              $email->number = $number;
              if($email->save()){
                  //Create cvs
                  // $allowedExts = array("pdf", "doc", "docx", "rtf", "tif", "txt");
                  $attachments = $message->getAttachments();
                  if($attachments){
                      foreach ($attachments as $attachment) {
                          $fileName = $attachment->getFilename();
                          if($fileName){
                              $extension = explode(".", $fileName);
                              $name = current($extension);
                              $type = end($extension);
                              // if(in_array(strtolower($type), $allowedExts)){
                              if(!in_array(strtolower($type), array("gif","png","jpg","jpeg", "htm"))){
                                  $cv = new Cvs();
                                  $cv->emailID = $email->getPrimaryKey();
                                  $clear = strip_tags($name);
                                  $clear = html_entity_decode($clear);
                                  $clear = urldecode($clear);
                                  $clear = preg_replace('/[^A-Za-z0-9]/', ' ', $clear);
                                  $clear = preg_replace('/ +/', ' ', $clear);
                                  $clear = trim($clear);
                                  $cv->name = $clear . '.' . $type;
                                  if($cv->save()){
                                      file_put_contents(
                                        Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'cvs/' . $email->getPrimaryKey() . '_v1_' . $cv->name,
                                        $attachment->getDecodedContent()
                                      );
                                  }
                              }
                              // }
                          }
                      }
                  }
                  $message->markAsSeen();
              }
          // } else {
          //   $message->getBodyHtml();//Mark as seen :)
          // }
          if($count++ >= $limit) break;
      }
    }

    public function actionParse()//Not use
    {
        $emails = Email::find()->where('status =' . Email::STATUS_NOTPARSE)->all();
        if($emails){
            // Create Rex service and parse cv
            foreach ($emails as $email) {
                $candList = array();
                // Each email
                $cvs = $email->cvs;
                if($cvs){//Parse from file
                    foreach ($cvs as $index => $cv) {
                        // Each resume
                        $content = "";
                        try {
                          $content = file_get_contents(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'cvs/' . $email->getPrimaryKey() . '_v1_' .$cv->name);
                        } catch (Exception $e) {
                            echo 'Caught exception: ',  $e->getMessage(), "\n";
                            //Set status parsed for email
                            $email->status = Email::STATUS_FALSE;
                            $email->save();
                        }
                        if($content){
                            $candName = uniqid();
                            if($candName){
                                $candList[$candName][] = $index;
                            }
                        }
                    }
                    if($candList){
                        foreach ($candList as $candidateName => $cvList) {
                            if($candidateName){
                                //Create task
                                $task = new Tasks();
                                $task->date = $email->date;
                                $task->subject = $email->subject;
                                $task->recruiter = $email->recruiter;
                                $task->address = $email->address;
                                $task->candidate = $candidateName;
                                $task->emailID = $email->id;
                                if($task->save()){
                                    $taskID = $task->getPrimaryKey();
                                    foreach ($cvList as $cvIndex) {
                                        $cvs[$cvIndex]->taskID = $taskID;
                                        $cvs[$cvIndex]->save();
                                    }
                                }
                            }
                        }
                    } else {
                        //Create task
                        $task = new Tasks();
                        $task->date = $email->date;
                        $task->subject = $email->subject;
                        $task->recruiter = $email->recruiter;
                        $task->address = $email->address;
                        $task->emailID = $email->id;
                        if($task->save()){
                            $taskID = $task->getPrimaryKey();
                            foreach ($email->cvs as $cv) {
                                $cv->taskID = $taskID;
                                $cv->save();
                            }
                        }
                    }
                    //Set status parsed for email
                    $email->status = Email::STATUS_PARSED;
                    $email->save();
                } else {//Parse from text content of email
                    $candName              = uniqid();
                    if($candName){
                        //Create task
                        $task = new Tasks();
                        $task->date = $email->date;
                        $task->subject = $email->subject;
                        $task->recruiter = $email->recruiter;
                        $task->address = $email->address;
                        $task->candidate = $candName;
                        $task->emailID = $email->id;
                        $task->save();

                        //Set status parsed for email
                        $email->status = Email::STATUS_PARSED;
                        $email->save();
                    }
                }
            }
        }
    }

    public function actionParseOld()//Not use
    {
        $emails = Email::find()->where('status =' . Email::STATUS_NOTPARSE)->all();
        if($emails){
            // Create Rex service and parse cv
            $rexParserService = new RexParserService(Yii::$app->params['rexIP']);
            $parser           = new CvParser($rexParserService);
            foreach ($emails as $email) {
                $candList = array();
                // Each email
                $cvs = $email->cvs;
                if($cvs){//Parse from file
                    foreach ($cvs as $index => $cv) {
                        // Each resume
                        $content = "";
                        try {
                          $content = file_get_contents(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'cvs/' . $email->getPrimaryKey() . '_v1_' .$cv->name);
                        } catch (Exception $e) {
                            echo 'Caught exception: ',  $e->getMessage(), "\n";
                            //Set status parsed for email
                            $email->status = Email::STATUS_FALSE;
                            $email->save();
                        }
                        if($content){
                          if ($parser->shortParse($content)) {
                              $candName              = $parser->getCandidate();
                              if($candName){
                                  $candList[$candName][] = $index;
                              }
                          }  
                        }
                    }
                    if($candList){
                        foreach ($candList as $candidateName => $cvList) {
                            if($candidateName){
                                //Create task
                                $task = new Tasks();
                                $task->date = $email->date;
                                $task->subject = $email->subject;
                                $task->recruiter = $email->recruiter;
                                $task->address = $email->address;
                                $task->candidate = $candidateName;
                                $task->emailID = $email->id;
                                if($task->save()){
                                    $taskID = $task->getPrimaryKey();
                                    foreach ($cvList as $cvIndex) {
                                        $cvs[$cvIndex]->taskID = $taskID;
                                        $cvs[$cvIndex]->save();
                                    }
                                }
                            }
                        }
                    } else {
                        //Create task
                        $task = new Tasks();
                        $task->date = $email->date;
                        $task->subject = $email->subject;
                        $task->recruiter = $email->recruiter;
                        $task->address = $email->address;
                        $task->emailID = $email->id;
                        if($task->save()){
                            $taskID = $task->getPrimaryKey();
                            foreach ($email->cvs as $cv) {
                                $cv->taskID = $taskID;
                                $cv->save();
                            }
                        }
                    }
                    //Set status parsed for email
                    $email->status = Email::STATUS_PARSED;
                    $email->save();
                } else {//Parse from text content of email
                    $mailContent = bzdecompress(base64_decode($email->text));
                        if ($parser->shortParse($mailContent)) {
                            $candName              = $parser->getCandidate();
                            if($candName){
                                //Create task
                                $task = new Tasks();
                                $task->date = $email->date;
                                $task->subject = $email->subject;
                                $task->recruiter = $email->recruiter;
                                $task->address = $email->address;
                                $task->candidate = $candName;
                                $task->emailID = $email->id;
                                $task->save();

                                //Set status parsed for email
                                $email->status = Email::STATUS_PARSED;
                                $email->save();
                            }
                        } else {
                            //Set status parsed for email
                            $email->status = Email::STATUS_FALSE;
                            $email->save();
                        }
                }
            }
        }
    }

    public function actionCraw(){
        $config = Yii::$app->params['imap'];
        $hostname = $config['host'];
        $username = $config['account'];
        $password = $config['password'];

        $inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());
            // $emails = imap_search($inbox,'UNSEEN',SE_UID);
            $emails = imap_search($inbox,'UNSEEN');
            if($emails){
              $max_emails = 16;
              $count = 1;
              rsort($emails);
              foreach($emails as $email_number)
              {
                  $content = imap_fetchbody($inbox,$email_number,1.2, FT_PEEK);
                  $content = base64_encode(bzcompress($content));
                  $email = new Email();
                  $email->date = $date;
                  $email->subject = $subject;
                  $email->recruiter = $recruiter;
                  $email->content = $content;
                  $email->number = $number;
                  $email->save();
                  exit;

                  $structure = imap_fetchstructure($inbox, $key+1);

                  $attachments = array();

                  if(isset($structure->parts) && count($structure->parts))
                  {
                      for($i = 0; $i < count($structure->parts); $i++)
                      {
                          $attachments[$i] = array(
                              'is_attachment' => false,
                              'filename' => '',
                              'name' => '',
                              'attachment' => ''
                          );

                          if($structure->parts[$i]->ifdparameters)
                          {
                              foreach($structure->parts[$i]->dparameters as $object)
                              {
                                  if(strtolower($object->attribute) == 'filename')
                                  {
                                      $attachments[$i]['is_attachment'] = true;
                                      $attachments[$i]['filename'] = $object->value;
                                  }
                              }
                          }

                          if($structure->parts[$i]->ifparameters)
                          {
                              foreach($structure->parts[$i]->parameters as $object)
                              {
                                  if(strtolower($object->attribute) == 'name')
                                  {
                                      $attachments[$i]['is_attachment'] = true;
                                      $attachments[$i]['name'] = $object->value;
                                  }
                              }
                          }

                          if($attachments[$i]['is_attachment'])
                          {
                              $attachments[$i]['attachment'] = imap_fetchbody($inbox, $key+1, $i+1);

                              if($structure->parts[$i]->encoding == 3)
                              {
                                  $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                              }
                              elseif($structure->parts[$i]->encoding == 4)
                              {
                                  $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                              }
                          }
                      }
                  }
                  foreach($attachments as $attachment)
                  {
                      if($attachment['is_attachment'] == 1)
                      {
                          $filename = $attachment['name'];
                          if(empty($filename)) $filename = $attachment['filename'];

                          if(empty($filename)) $filename = time() . ".dat";

                          $fp = fopen(Yii::getAlias('@web') . '/cvs/' . $filename, "w+");
                          fwrite($fp, $attachment['attachment']);
                          fclose($fp);
                      }

                  }

                  if($count++ >= $max_emails) break;
              }
              imap_close($inbox);
            }
    }
}
