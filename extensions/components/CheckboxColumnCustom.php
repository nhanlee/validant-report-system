<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace ext\components;

use Closure;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\grid\CheckboxColumn;

class CheckboxColumnCustom extends CheckboxColumn
{
    
    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->checkboxOptions instanceof Closure) {
            $options = call_user_func($this->checkboxOptions, $model, $key, $index, $this);
        } else {
            $options = $this->checkboxOptions;
            if (!isset($options['value'])) {
                $options['value'] = is_array($key) ? json_encode($key, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) : $key;
            }
        }

        return '<div style="text-align: center" class="checkbox"><label>' . Html::checkbox($this->name, !empty($options['checked']), $options) . '</label></div>';
    }
}
