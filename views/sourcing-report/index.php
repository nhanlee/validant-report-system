<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Sourcer;
use app\models\Clients;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SourcingReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sourcing Reports';
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('inputFinish')): ?>
        <div class="alert alert-success">
            <?php echo Yii::$app->session->getFlash('inputFinish'); ?>
        </div>
    <?php endif; ?>

    <?php  echo $this->render('create', ['model' => $model]); ?>
<hr>
<div class="sourcing-report-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'attribute' => 'added',
                'filter' => false,
                'format' => ['date', 'php:d M, Y']
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Type',
                'content' => function ($model, $key, $index, $column)
                {
                    return $model->sourcingType->name;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'sourcerName',
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'clientName',
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'teamName',
                'filter' => false,
            ],
            'candidate',
            'note',
            // 'created_by',
            // 'added',
            // 'updated',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}'],
        ],
    ]); ?>

</div>
