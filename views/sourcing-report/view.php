<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SourcingReport */

$this->title = $model->candidate;
$this->params['breadcrumbs'][] = ['label' => 'Sourcing Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $team = Yii::$app->params['team']; ?>

<div class="sourcing-report-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if(Yii::$app->user->identity->group == 1): ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [                      // the owner name of the model
                'label' => 'Type',
                'value' => $model->sourcingType->name,
            ],
            [                      // the owner name of the model
                'label' => 'Sourcer',
                'value' => $model->sourcer->getText(),
            ],
            [                      // the owner name of the model
                'label' => 'Recruiter',
                'value' => $model->client->client_name,
            ],
            [                      // the owner name of the model
                'label' => 'Team',
                'value' => $team[$model->team],
            ],
            'candidate',
            'note',
            [                      // the owner name of the model
                'label' => 'Date',
                'value' => date_format(new DateTime($model->added), 'd M, Y'),
            ],
            [                      // the owner name of the model
                'label' => 'Updated',
                'value' => date_format(new DateTime($model->updated), 'd M, Y'),
            ]
        ],
    ]) ?>

</div>
