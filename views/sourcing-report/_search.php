<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Clients;

use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\SourcingReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
  $team = Yii::$app->params['team'];
  unset($team[4]);
  unset($team[5]);
?>

<div class="sourcing-report-search clearfix">

    <?php $form = ActiveForm::begin([
        'action' => ['report'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'team')->dropDownList(
                                                    $team,
                                                    ['prompt' => '--Choose a team--']
                                                ) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 clearfix">
            <?= $form->field($model, 'added')->widget(DateRangePicker::classname(),[
                'presetDropdown'=>true,
                'hideInput'=>false,
                'convertFormat'=>true,
                'pluginOptions'=>[
                    'timePicker'=>false,
                    'separator'=>' -- ',
                    'format'=>'Y-m-d'
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group text-right col-md-4">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
