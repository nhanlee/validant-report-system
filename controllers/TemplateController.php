<?php

namespace app\controllers;

use Yii;
use app\models\Template;
use app\models\TemplateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TemplateController implements the CRUD actions for Template model.
 */
class TemplateController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Template models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tasks model.
     * @param integer $id
     * @return mixed
     */
    public function actionSend()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //pick ok or not by sourcer
        $msg = '';
        $success = false;
        if(Yii::$app->user->identity->group == 1){
            $from = Yii::$app->params['noreplyEmail'];
            $cc = array();
            $to = array();
            if(isset($_POST['toEmail']) && $_POST['toEmail'] != ''){
                $to = explode(',', $_POST['toEmail']);//array to
                $to = array_map('trim', $to);
            }
            if(isset($_POST['ccEmail']) && $_POST['ccEmail'] != ''){
                $cc = explode(',', $_POST['ccEmail']);//array cc
                $cc = array_map('trim', $cc);
            }
            if(YII_DEBUG){
                $to = Yii::$app->params['adminEmail'];
                $cc = Yii::$app->params['adminEmail'];
            }
            $message = Yii::$app->critmailer->compose();
            if(count($cc) > 0){
                $message->setCc($cc);
            }
            $message->setFrom($from)
                    ->setTo($to)
                    ->setSubject($_POST['subjectEmail'])
                    ->setTextBody('Plain text content')
                    ->setHtmlBody($_POST['contentEmail'])
                    ->send();
            $msg = 'Well done, You have sent email!';
            $success = true;
        } else {
            $msg = "Oops, Please contact Administrator, thanks!";
            $success = false;
        }

        $data = [
            'success'   => $success,
            'data'      => [
                'msg' =>$msg
            ]

        ];
        return $data;
    }

    /**
     * Displays a single Template model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Template model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Template();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Template model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Template model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Template model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Template the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Template::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
