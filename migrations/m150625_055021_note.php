<?php

use yii\db\Schema;
use yii\db\Migration;

class m150625_055021_note extends Migration
{
    public function up()
    {
        $sql="ALTER TABLE `sourcing_report` 
        ADD COLUMN `note` VARCHAR(255) NULL AFTER `updated`;";
        $this->execute($sql);
        echo "Alter done" .'\n';

        $sql="ALTER TABLE `expense_report` 
        ADD COLUMN `note` VARCHAR(255) NULL AFTER `updated`;";
        $this->execute($sql);
        echo "Alter done" .'\n';
    }

    public function down()
    {
        echo "m150625_055021_note cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
