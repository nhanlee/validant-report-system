<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'aliases' => [
        '@ext' => '@app/extensions',
    ],
    // 'defaultRoute' => 'tasks',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'enableCookieValidation' => false,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@runtime/cache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            // 'useFileTransport' => true,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => $params['imap']['smtp'],
                'username' => $params['imap']['account'],
                'password' => $params['imap']['password'],
                'port' => '465',
                'encryption' => 'ssl',
                // 'port' => '587',
                // 'encryption' => 'tls',
            ],
        ],
        'critmailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'super1.critsend.com',
                'username' => 'jgalvin@glandoresystems.com',
                'password' => 'FzgWtvwT8QsfPc8',
                'port' => '25',
                'encryption' => NULL
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'legacyDB' => require(__DIR__ . '/LegacyDB.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            //'showScriptName' => false, //index.php
            'rules' => [
                '' => 'site/index',
            ],
            // ...
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
