<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sourcing_type".
 *
 * @property integer $id
 * @property string $name
 */
class SourcingType extends \yii\db\ActiveRecord
{
    const TYPE_SOURCING         = 1;
    const TYPE_EXPENSE          = 2; 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sourcing_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
