<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property integer $client_id
 * @property string $client_name
 * @property string $client_email
 * @property integer $client_active
 * @property integer $client_added
 * @property integer $client_updated
 */
class Clients extends \yii\db\ActiveRecord
{
    const ACTIVE         = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_name'], 'required'],
            [['client_active', 'client_added', 'client_updated'], 'integer'],
            [['client_name'], 'string', 'max' => 255],
            [['client_email'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Client ID',
            'client_name' => 'Client Name',
            'client_email' => 'Client Email',
            'client_active' => 'Client Active',
            'client_added' => 'Client Added',
            'client_updated' => 'Client Updated',
        ];
    }
}
